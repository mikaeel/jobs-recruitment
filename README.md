# Jobs Recruitment

This system allow job recruiters to manage the whole process of job application with easy.

Click [here](https://udom-rs.herokuapp.com/) to view the demo

## How to install

- Clone the repository

  `git clone url`

- Install composer dependencies

  `composer install`
  
- Migrate the database
  
  `php artisan migrate`

- Run the database seeders
  
  `php artisan db:seed`
  
- You are done

## How to login

- email: hr@udom.ac.tz
- password: secret

## How it works

- HR post a job position

- User sees the job position

- User login or create account and login

- User apply for that job position

- HR views the applicant in that particular job position

- HR views the Curicullum Vitae of each applicant

- HR call for interview for applicants who has met requirements

### Note

This is a work in progress