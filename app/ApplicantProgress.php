<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicantProgress extends Model
{
    protected $fillable = ['personal_details'];
}
