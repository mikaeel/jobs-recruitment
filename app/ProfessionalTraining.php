<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfessionalTraining extends Model
{
    protected $dates = [
        'training_date',
    ];
}
