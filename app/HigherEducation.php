<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HigherEducation extends Model
{
    public function qualification()
    {
    	return $this->belongsTo(Qualification::class);
    }
}
