<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $table = 'job_vacancies';

    protected $dates = ['closed_at'];

    public function applicants()
    {
        return $this->belongsToMany(User::class, 'job_vacancy_user', 'job_vacancy_id', 'user_id')->withTimestamps()->withPivot('called_for_interview');
    }

    public function jobCategory()
    {
    	return $this->belongsTo(JobCategory::class);
    }
}
