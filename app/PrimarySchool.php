<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrimarySchool extends Model
{
    protected $fillable = [
    	'user_id',
    	'name',
    	'completed',
    	'region',
    	'district',
    	'certificate',
    ];
}
