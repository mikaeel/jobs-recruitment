<?php

namespace App;

use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'gender',
        'marital_status',
        'dob',
        'language_proficiency',
        'address',
        'phone',
        'email',
        'place_of_domicile',
        'current_location',
        'nationality',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($user) {
            $user->token = str_random(30);
        });
    }


    public function confirmEmail()
    {
        $this->verified = true;
        $this->token = null;

        $this->save();
    }
    
    public function computerSkills()
    {
        return $this->belongsToMany(ComputerSkill::class)->withPivot('competence_id');
    }

    public function educationBackgrounds()
    {
        return $this->hasMany(EducationBackground::class);
    }

    public function references()
    {
        return $this->hasMany(Reference::class);
    }

    public function interests()
    {
        return $this->hasMany(Interest::class);
    }

    public function employmentHistories()
    {
        return $this->hasMany(EmploymentHistory::class);
    }

    public function professionalTrainings()
    {
        return $this->hasMany(ProfessionalTraining::class);
    }

    public function additionalSkills()
    {
        return $this->hasMany(AdditionalSkill::class);
    }

    public function job_applications()
    {
        return $this->belongsToMany(Job::class, 'job_vacancy_user', 'user_id', 'job_vacancy_id' )->withTimestamps()->withPivot('called_for_interview');
    }

    public function progress()
    {
        return $this->hasOne(UserProgress::class);
    }

    public function totalProgress()
    {
        $total = 0;
        if ($this->progress()->first()->personal_details) {
            $total += 1;
        }
        if ($this->progress()->first()->education_background) {
            $total += 1;
        }
        if ($this->progress()->first()->employment_history) {
            $total += 1;
        }
        if ($this->progress()->first()->additional_skills) {
            $total += 1;
        }
        if ($this->progress()->first()->professional_training) {
            $total += 1;
        }        
        if ($this->progress()->first()->computer_skills) {
            $total += 1;
        }
        if ($this->progress()->first()->interests) {
            $total += 1;
        }
        if ($this->progress()->first()->references) {
            $total += 1;
        }

        return $total;
    }

    public function primarySchool()
    {
        return $this->hasOne(PrimarySchool::class);
    }

    public function ordinaryLevel()
    {  
       return $this->hasOne(OrdinaryLevel::class); 
    }

    public function advancedLevel()
    {  
       return $this->hasOne(AdvancedLevel::class); 
    }

    public function progressStatus()
       {
           return $this->progress()->first()->education_background && $this->progress()->first()->personal_details && $this->progress()->first()->interests && $this->progress()->first()->references;
       }  

       public function higherEducation()
        {
            return $this->hasMany(HigherEducation::class);
        } 
}
