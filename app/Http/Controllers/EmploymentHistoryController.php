<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\EmploymentHistory;
use Illuminate\Http\Request;

class EmploymentHistoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $employmentHistories = auth()->user()->employmentHistories()->get();
        return view('employment-history.index', compact('employmentHistories'));
    }

    public function create()
    {
        return view('employment-history.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
                'company_name' => 'required',
                'address' => 'required',
                'phone' => 'required',
                'job_title' => 'required',
                'job_responsibilities' => 'required',
                'starts_at' => 'required',
                'ends_at' => 'required',
            ]);

        $employmentHistory = new EmploymentHistory();
        $employmentHistory->company_name = $request->input('company_name');
        $employmentHistory->address = $request->input('address');
        $employmentHistory->phone = $request->input('phone');
        $employmentHistory->job_title = $request->input('job_title');
        $employmentHistory->job_responsibilities = $request->input('job_responsibilities');
        $employmentHistory->starts_at = $request->input('starts_at');
        $employmentHistory->ends_at = $request->input('ends_at');
        $employmentHistory->user_id = auth()->id();
        $employmentHistory->save();

        $progress = Auth::user()->progress()->first();
        $progress->employment_history = true;
        $progress->save();         

        flash('Success')->success();

        return redirect('employment-history');
    }

    public function edit($id)
    {
        $employmentHistory = EmploymentHistory::findOrFail($id);

        return view('employment-history.edit', compact('employmentHistory'));
    }
}
