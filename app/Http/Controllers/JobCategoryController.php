<?php

namespace App\Http\Controllers;

use App\JobCategory;
use Illuminate\Http\Request;

class JobCategoryController extends Controller
{
    public function show($slug)
    {
    	$jobCategories = JobCategory::get();

    	$jobCategory = JobCategory::whereSlug($slug)->firstOrFail();

    	$jobs = $jobCategory->jobs()->with('jobCategory')->latest()->paginate(5);

    	return view('jobs.index', compact('jobCategories', 'jobs'));
    }
}
