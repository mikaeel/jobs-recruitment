<?php

namespace App\Http\Controllers;

use App\Job;
use App\JobCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JobController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index()
    {
        $jobCategories = JobCategory::get();
        $jobs = Job::with('jobCategory')->latest()->paginate(5);
        return view('jobs.index', compact('jobCategories', 'jobs'));
    }

    public function show($id)
    {
        $job = Job::with(['jobCategory', 'applicants'])->findOrFail($id);

        $applicantJob = new Job();

        if (Auth::check()) {
            $applicantJob = Job::whereHas('applicants', function ($query) use ($job) {
                $query->where('user_id', auth()->id())->where('job_vacancy_id', $job->id);
            })->get();
        }

        return view('jobs.show', compact('job', 'applicantJob'));
    }
}
