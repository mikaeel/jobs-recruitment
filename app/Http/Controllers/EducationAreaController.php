<?php

namespace App\Http\Controllers;

use App\EducationArea;
use Illuminate\Http\Request;

class EducationAreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EducationArea  $educationArea
     * @return \Illuminate\Http\Response
     */
    public function show(EducationArea $educationArea)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EducationArea  $educationArea
     * @return \Illuminate\Http\Response
     */
    public function edit(EducationArea $educationArea)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EducationArea  $educationArea
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EducationArea $educationArea)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EducationArea  $educationArea
     * @return \Illuminate\Http\Response
     */
    public function destroy(EducationArea $educationArea)
    {
        //
    }
}
