<?php

namespace App\Http\Controllers\Dashboard;

use App\Qualification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QualificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $qualifications = Qualification::orderBy('name')->get();
        return view('dashboard.qualifications.index', compact('qualifications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.qualifications.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
        ]);

        $qualification = new Qualification;

        $qualification->name = $request->name;
        $qualification->description = $request->description;
        $qualification->save();

        flash('Qualification Level posted successfully')->success();

        return redirect('/dashboard/qualification-level');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Qualification  $qualification
     * @return \Illuminate\Http\Response
     */
    public function show(Qualification $qualification)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Qualification  $qualification
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $qualification = Qualification::findOrFail($id);
        return view('dashboard.qualifications.edit', compact('qualification'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Qualification  $qualification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
        ]);

        $qualification = Qualification::findOrFail($id);

        $qualification->name = $request->name;
        $qualification->description = $request->description;
        $qualification->save();

        flash('Qualification Level updated successfully')->success();

        return redirect('/dashboard/qualification-level');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Qualification  $qualification
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $qualification = Qualification::findOrFail($id);

        $qualification->delete();

        flash('Qualification Level deleted successfully')->success();

        return redirect('/dashboard/qualification-level');

    }
}
