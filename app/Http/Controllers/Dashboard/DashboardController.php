<?php

namespace App\Http\Controllers\Dashboard;

use App\Job;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        $jobsCount = Job::count();
        $usersCount = User::count();

        $jobs = Job::withCount('applicants')->with('applicants')->has('applicants')->get();

        $jobsWithApplicantsCalledForInterview = Job::has('applicants')->with(['applicants' => function ($query) {
        	$query->where('called_for_interview', 1)->has('job_applications');
        }])->get();

        $jobsWithApplicantsNotCalledForInterview = Job::has('applicants')->with(['applicants' => function ($query) {
        	$query->where('called_for_interview', 0);
        }])->get();

        return view('dashboard.index', compact('jobsCount', 'usersCount', 'jobs', 'jobsWithApplicantsCalledForInterview', 'jobsWithApplicantsNotCalledForInterview'));
    }
}
