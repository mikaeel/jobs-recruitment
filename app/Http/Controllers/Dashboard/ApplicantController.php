<?php

namespace App\Http\Controllers\Dashboard;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApplicantController extends Controller
{
    public function index()
    {
    	$applicants = User::with('job_applications')->has('job_applications')->withCount('job_applications')->latest()->paginate(5);
    	return view('dashboard.applicants.index', compact('applicants'));
	}

	public function show($id)
	{
		$applicant = User::with('job_applications')->findOrFail($id);
		return view('dashboard.applicants.show', compact('applicant'));
	}

	public function profile($id)
	{
		$applicant = User::with('job_applications')->findOrFail($id);
		return view('dashboard.applicants.profile', compact('applicant'));
	}
}
