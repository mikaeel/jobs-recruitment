<?php

namespace App\Http\Controllers\Dashboard;

use App\Job;
use App\User;
use App\JobCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JobController extends Controller
{
    public function index()
    {
        $jobs = Job::withCount(['applicants' => function ($query) {
            $query->has('ordinaryLevel');
        }])->latest()->get();

        return view('dashboard.jobs.index', compact('jobs'));
    }

    public function create()
    {
        $jobCategories = JobCategory::get();
        return view('dashboard.jobs.create', compact('jobCategories'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'job_category_id' => 'required',
            'body' => 'required',
            'positions' => 'required',
        ]);

        $jobVacancy = new Job();
        $jobVacancy->title = $request->input('title');
        $jobVacancy->slug = str_slug($request->input('title'), '-');
        $jobVacancy->body = nl2br(htmlentities($request->input('body'), ENT_QUOTES, 'UTF-8'));
        $jobVacancy->positions = $request->input('positions');
        $jobVacancy->user_id = auth()->id();
        $jobVacancy->status = false;
        $jobVacancy->closed_at = $request->input('closed_at');
        $jobVacancy->job_category_id = $request->input('job_category_id');
        $jobVacancy->save();

        flash('Job vacancy posted successfully')->success();

        return redirect()->route('dashboard.jobs.index');
    }

    public function show($slug, $id)
    {
        $job = Job::with('applicants')->whereId($id)->whereSlug($slug)->firstOrFail();

        $applicants = $job->applicants()->has('ordinaryLevel')->get();

        return view('dashboard.jobs.show', compact('job', 'applicants'));
    }

    public function applicants($slug, $jobId, $applicantId)
    {
        $applicant = User::with('job_applications')->findOrFail($applicantId);
        $job = Job::whereId($jobId)->whereSlug($slug)->firstOrFail();

        return view('dashboard.applicants.show', compact('applicant', 'job'));
    }

    public function edit($id)
    {
        $job = Job::findOrFail($id);
        $jobCategories = JobCategory::get();
        return view('dashboard.jobs.edit', compact('job', 'jobCategories'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'job_category_id' => 'required',
            'body' => 'required',
            'positions' => 'required',
        ]);

        $jobVacancy = Job::findOrFail($id);

        $jobVacancy->title = $request->input('title');
        $jobVacancy->slug = str_slug($request->input('title'), '-');
        $jobVacancy->body = nl2br(htmlentities($request->input('body'), ENT_QUOTES, 'UTF-8'));
        $jobVacancy->positions = $request->input('positions');
        $jobVacancy->user_id = auth()->id();
        $jobVacancy->status = false;
        $jobVacancy->closed_at = $request->input('closed_at');
        $jobVacancy->job_category_id = $request->input('job_category_id');
        $jobVacancy->save();

        flash('Job vacancy updateed successfully')->success();

        return redirect()->route('dashboard.jobs.index');        
    }
}
