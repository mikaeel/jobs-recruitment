<?php

namespace App\Http\Controllers\Dashboard;

use App\Job;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InterviewController extends Controller
{
    public function store($jobId, $applicantId)
    {
        $job = Job::findOrFail($jobId);
        $applicant = User::findOrFail($applicantId);

        $pivot = $job->applicants()->whereUserId($applicant->id)->first()->pivot;
        $pivot->called_for_interview = true;
        $pivot->save();

        flash('Applicant successfully called for interview.')->success()->important();

        return redirect()->route('dashboard.jobs.show', [$job->slug, $job->id]);
    }
}
