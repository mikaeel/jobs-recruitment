<?php

namespace App\Http\Controllers;

use App\Reference;
use Illuminate\Http\Request;

class ReferenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $references = auth()->user()->references()->get();
        return view('references.index', compact('references'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('references.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
                'title' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'phone' => 'required',
                'email' => 'required',
                'institution' => 'required',
                'address' => 'required',
            ]);

        $reference = new Reference();
        $reference->title = $request->input('title');
        $reference->first_name = $request->input('first_name');
        $reference->middle_name = $request->input('middle_name');
        $reference->last_name = $request->input('last_name');
        $reference->phone = $request->input('phone');
        $reference->email = $request->input('email');
        $reference->institution = $request->input('institution');
        $reference->address = $request->input('address');
        $reference->user_id = auth()->id();
        $reference->save();

        $progress = auth()->user()->progress()->first();
        $progress->references = true;
        $progress->save();         

        flash('Reference added successfully')->success();

        return redirect('references');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reference $reference
     * @return \Illuminate\Http\Response
     */
    public function show(Reference $reference)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reference $reference
     * @return \Illuminate\Http\Response
     */
    public function edit(Reference $reference)
    {
        return view('references.edit', compact('reference'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Reference $reference
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reference $reference)
    {
        $this->validate($request, [
                'title' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'phone' => 'required',
                'email' => 'required',
                'institution' => 'required',
                'address' => 'required',
            ]);
        
        $reference->title = $request->input('title');
        $reference->first_name = $request->input('first_name');
        $reference->middle_name = $request->input('middle_name');
        $reference->last_name = $request->input('last_name');
        $reference->phone = $request->input('phone');
        $reference->email = $request->input('email');
        $reference->institution = $request->input('institution');
        $reference->address = $request->input('address');
        $reference->user_id = auth()->id();
        $reference->save();

        flash('Reference successfully updated.')->success();

        return redirect('references');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reference $reference
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reference $reference)
    {
        $reference->delete();

        flash('Reference successfully deleted.')->success();

        return redirect('references');
    }
}
