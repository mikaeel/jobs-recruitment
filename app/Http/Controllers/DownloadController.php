<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DownloadController extends Controller
{
    public function download()
    {
    	$path = storage_path('app/' . request()->filepath);
    	return response()->download($path);
    }

    public function preview()
    {
    	$path = storage_path('app/' . request()->filepath);
    	return response()->file($path);
    }    
}
