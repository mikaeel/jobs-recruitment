<?php

namespace App\Http\Controllers;

use App\ProfessionalTraining;
use App\User;
use Illuminate\Http\Request;

class ProfessionalTrainingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $professionalTrainings = auth()->user()->professionalTrainings()->get();
        return view('professional-training.index', compact('professionalTrainings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('professional-training.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
                'name' => 'required',
                'training_date' => 'required',
            ]);

        $pt = new ProfessionalTraining();
        $pt->name = $request->input('name');
        $pt->description = $request->input('description');
        $pt->training_date = $request->input('training_date');
        $pt->user_id = auth()->id();
        $pt->save();

        $progress = auth()->user()->progress()->first();
        $progress->professional_training = true;
        $progress->save();        

        flash('Added Successfully')->success();

        return redirect('professional-training');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProfessionalTraining $professionalTraining
     * @return \Illuminate\Http\Response
     */
    public function show(ProfessionalTraining $professionalTraining)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProfessionalTraining $professionalTraining
     * @return \Illuminate\Http\Response
     */
    public function edit(ProfessionalTraining $professionalTraining)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\ProfessionalTraining $professionalTraining
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProfessionalTraining $professionalTraining)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProfessionalTraining $professionalTraining
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProfessionalTraining $professionalTraining)
    {
        //
    }
}
