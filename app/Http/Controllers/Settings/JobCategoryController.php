<?php

namespace App\Http\Controllers\Settings;

use App\JobCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JobCategoryController extends Controller
{
    public function index()
    {
    	$jobCategories = JobCategory::get();
    	return view('settings.job-categories.index', compact('jobCategories'));
    }

    public function create()
    {
    	return view('settings.job-categories.create');
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
    		'name' => 'required',
    	]);

    	$jobCategory = new JobCategory;
    	$jobCategory->name = $request->name;
    	$jobCategory->slug = str_slug($request->name);
    	$jobCategory->user_id = auth()->id();
    	$jobCategory->save();

    	flash('Success')->success()->important();

    	return redirect()->route('job-categories.index');
    }
}
