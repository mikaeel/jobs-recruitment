<?php

namespace App\Http\Controllers\Settings;

use App\ComputerSkill;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
class ComputerSkillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $computerSkills = ComputerSkill::latest()->get();
        return view('settings.computer-skills.index', compact('computerSkills'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         
        return view('settings.computer-skills.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|unique:computer_kills',
            ]);
    $ComputerSkill = new ComputerSkill();
    $ComputerSkill->name = $request->input('name');
    $ComputerSkill->description = $request->input('description');
    $ComputerSkill->slug = str_slug($request->input('name'), '-');
    $ComputerSkill->user_id = Auth::user()->id;
    $ComputerSkill->save();
    flash('added successifully')->success()->important();
    return redirect('settings/computer-skills');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    $computerSkills = ComputerSkill::find($id);
     return view('settings.computer-skills.edit',compact('computerSkills'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
            ]);
     $computerskill = ComputerSkill::find($id);
     $computerskill->name = $request->input('name');
     $computerskill->description = $request->input('description');
     $computerskill->slug = str_slug($request->input('name'), '-');
     $computerskill->user_id = Auth::user()->id;
     $computerskill->save();
     flash('Updated successifully')->success()->important();
     return redirect('settings/computer-skills');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ComputerSkill $computerSkill)
    {

        $computerSkill->delete();
        
        flash('Deleted succesifully')->success()->important();
        
        return back();
    }
}
