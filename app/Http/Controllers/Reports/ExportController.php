<?php

namespace App\Http\Controllers\Reports;

use Excel;
use App\Job;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExportController extends Controller
{
    public function jobApplicants(Job $job)
    {
        $applicantIds = $job->applicants()->where('called_for_interview', 1)->orderBy('users.first_name')->pluck('users.id');
        
        $applicants = User::has('ordinaryLevel')->find($applicantIds);

        $users = [];

        foreach ($applicants as $applicant) {
            $user = new User;
            $user->first_name = $applicant->first_name;
            $user->middle_name = $applicant->middle_name;
            $user->last_name = $applicant->last_name;
            $user->dob = $applicant->dob;
            $user->gender = $applicant->gender;
            $user->f4_index = $applicant->ordinaryLevel->index_number;
            if ($applicant->advancedLevel){
                $user->f6_index = $applicant->advancedLevel->index_number;
            }
            else{
                $user->f6_index = null;
            }           

            $users[] = $user;
        }

        $applicants = collect($users);


        \Excel::create('Applicants for ' . $job->title, function ($excel) use ($applicants) {
            $excel->sheet('Applicants', function ($sheet) use ($applicants) {
                $sheet->fromModel($applicants);
            });
        })->download('xlsx');

        return back();    	
    }

    public function jobsWithApplicants()
    {
        $jobsWithApplicants = Job::with('applicants')->has('applicants')->get();

        return Excel::create('New file', function($excel) use ($jobsWithApplicants) {

            foreach ($jobsWithApplicants as $jobsWithApplicant) {

                $excel->sheet('Sheet', function($sheet) use ($jobsWithApplicant) {

                    $data = [];

                    $data[] = [$jobsWithApplicant->title];

                    $data[] = ['First name', 'Middle name', 'Last name', 'Gender', 'Date of birth', 'Nationality', 'Phone', 'Email address', 'F4 Index number', 'F6 Index number'];

                    foreach ($jobsWithApplicant->applicants as $applicant) {
                        $data[] = [
                        $applicant->first_name, 
                        $applicant->middle_name,
                        $applicant->last_name,
                        $applicant->gender,
                        $applicant->dob,
                        $applicant->nationality,
                        $applicant->phone,
                        $applicant->email,
                        $applicant->ordinaryLevel !== null ? $applicant->ordinaryLevel->index_number : null,
                        $applicant->advancedLevel !== null ? $applicant->advancedLevel->index_number : null,
                        ];
                    }

                    $sheet->fromArray($data);
                    $sheet->mergeCells('A2:J2');

                });
            }

        })->download('xlsx');
    }

    public function jobsWithApplicantsCallForInterview()
    {
        $jobsWithApplicants = Job::has('applicants')->with(['applicants' => function ($query) {
            $query->where('called_for_interview', 1)->has('job_applications');
        }])->get();

        return Excel::create('New file', function($excel) use ($jobsWithApplicants) {

            foreach ($jobsWithApplicants as $jobsWithApplicant) {

                $excel->sheet('Sheet', function($sheet) use ($jobsWithApplicant) {

                    $data = [];

                    $data[] = [$jobsWithApplicant->title];

                    $data[] = ['First name', 'Middle name', 'Last name', 'Gender', 'Date of birth', 'Nationality', 'Phone', 'Email address', 'F4 Index number', 'F6 Index number'];

                    foreach ($jobsWithApplicant->applicants as $applicant) {
                        $data[] = [
                        $applicant->first_name, 
                        $applicant->middle_name,
                        $applicant->last_name,
                        $applicant->gender,
                        $applicant->dob,
                        $applicant->nationality,
                        $applicant->phone,
                        $applicant->email,
                        $applicant->ordinaryLevel !== null ? $applicant->ordinaryLevel->index_number : null,
                        $applicant->advancedLevel !== null ? $applicant->advancedLevel->index_number : null,
                        ];
                    }

                    $sheet->fromArray($data);
                    $sheet->mergeCells('A2:J2');

                });
            }

        })->download('xlsx');
    }

    public function jobsWithApplicantsNotCallForInterview()
    {
        $jobsWithApplicants = Job::has('applicants')->with(['applicants' => function ($query) {
            $query->where('called_for_interview', 0)->has('job_applications');
        }])->get();

        return Excel::create('New file', function($excel) use ($jobsWithApplicants) {

            foreach ($jobsWithApplicants as $jobsWithApplicant) {

                $excel->sheet('Sheet', function($sheet) use ($jobsWithApplicant) {

                    $data = [];

                    $data[] = [$jobsWithApplicant->title];

                    $data[] = ['First name', 'Middle name', 'Last name', 'Gender', 'Date of birth', 'Nationality', 'Phone', 'Email address', 'F4 Index number', 'F6 Index number'];

                    foreach ($jobsWithApplicant->applicants as $applicant) {
                        $data[] = [
                        $applicant->first_name, 
                        $applicant->middle_name,
                        $applicant->last_name,
                        $applicant->gender,
                        $applicant->dob,
                        $applicant->nationality,
                        $applicant->phone,
                        $applicant->email,
                        $applicant->ordinaryLevel !== null ? $applicant->ordinaryLevel->index_number : null,
                        $applicant->advancedLevel !== null ? $applicant->advancedLevel->index_number : null,
                        ];
                    }

                    $sheet->fromArray($data);
                    $sheet->mergeCells('A2:J2');

                });
            }

        })->download('xlsx');
    }
}
