<?php

namespace App\Http\Controllers;

use App\EducationBackground;
use App\EducationArea;
use App\EducationLevel;
use Illuminate\Http\Request;

class EducationBackgroundController extends Controller
{
    public function index()
    {
        $educationBackgrounds = auth()->user()->educationBackgrounds()->get();
        return view('education-background.index', compact('educationBackgrounds'));
    }

    public function create()
    {
        $educationLevels = EducationLevel::orderBy('name')->get();
        $educationAreas = EducationArea::orderBy('name')->get();
        return view('education-background.create', compact('educationLevels', 'educationAreas'));
    }

    public function store(Request $request)
    {
        $educationBackground = new EducationBackground();
        $educationBackground->education_level_id = $request->input('education_level_id');
        $educationBackground->education_area_id = $request->input('education_area_id');
        $educationBackground->notes = $request->input('notes');
        $educationBackground->starts_at = $request->input('starts_at');
        $educationBackground->ends_at = $request->input('ends_at');
        $educationBackground->user_id = auth()->id();
        $educationBackground->save();

        $progress = Auth::user()->progress->first();
        $progress->education_background = true;
        $progress->save();        

        flash('Education Level Added Successfully')->success();

        return redirect('education-background');
    }
}
