<?php

namespace App\Http\Controllers;

use App\Job;
use App\User;
use App\Nationality;
use App\ApplicantProgress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
    {
        $nationalities = Nationality::get();
        $user = Auth::user();
        return view('personal-details.show', compact('nationalities', 'user'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'middle_name' => 'required',
            'last_name' => 'required',
            'dob' => 'required',
            'gender' => 'required',
            'nationality' => 'required',
            'phone' => 'required',
            'place_of_domicile' => 'required',
            'current_location' => 'required',
            'address' => 'required',
        ]);

        $user = Auth::user();

        $user->update($request->all());

        $user->save();

        $progress = Auth::user()->progress()->first();
        $progress->personal_details = true;
        $progress->save();

        flash('Success! All personal information saved successfully.')->success();

        return back();
    }

    public function myCv()
    {
        $user = User::has('ordinaryLevel')
                        ->find(auth()->id());

        if(!$user) {
            flash('Please complete your profile before previewing your CV')->warning();
            return back();
        }

        return view('user.my-cv', compact('user'));
    }

    public function storeJob($slug, $id)
    {
        $applicant = auth()->user();
        $job = Job::whereSlug($slug)->whereId($id)->firstOrFail();

        $job->applicants()->attach($applicant);

        flash('Job successfully applied.');

        return redirect()->home();
    }
}
