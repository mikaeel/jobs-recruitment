<?php

namespace App\Http\Controllers;

use App\HigherEducation;
use Illuminate\Http\Request;
use App\Qualification;
use Auth;

class HigherEducations extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $higherEducations = HigherEducation::with('qualification')->latest('created_at')->get();
        $higherEducations = auth()->user()->higherEducation;

        return view('higher-education.index', compact('higherEducations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $qualifications = Qualification::orderBy('name')->get();

        return view('higher-education.create', compact('qualifications'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
                'registration_number' => 'required',
                'program_name' => 'required',
                'graduation_year' => 'required',
                'overall_gpa' => 'required',
                'certificate' => 'required',
                'qualification_id' => 'required',
            ]);
        $higherEducation = new HigherEducation;
        $higherEducation->registration_number = $request->registration_number;
        $higherEducation->program_name = $request->program_name;
        $higherEducation->graduation_year = $request->graduation_year;
        $higherEducation->overall_gpa = $request->overall_gpa;
        $higherEducation->qualification_id = $request->qualification_id;
        $higherEducation->user_id = Auth::user()->id;

        if ($request->hasFile('certificate')) {
            if ($request->file('certificate')->isValid()) {
                $path = $request->certificate->store('higher-education', '');
                $higherEducation->certificate = $path;
            }
        }

        $higherEducation->save();

        flash('Higher Education added successfully')->success();

        return redirect('/higher-education');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HigherEducation  $higherEducation
     * @return \Illuminate\Http\Response
     */
    public function show(HigherEducation $higherEducation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HigherEducation  $higherEducation
     * @return \Illuminate\Http\Response
     */
    public function edit(HigherEducation $higherEducation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HigherEducation  $higherEducation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HigherEducation $higherEducation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HigherEducation  $higherEducation
     * @return \Illuminate\Http\Response
     */
    public function destroy(HigherEducation $higherEducation)
    {
        //
    }
}
