<?php

namespace App\Http\Controllers;

use App\Competence;
use App\ComputerSkill;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ComputerSkillController extends Controller
{
    public function index()
    {
        $competences = Competence::all();
        $computerSkills = ComputerSkill::get();
        $userComputerSkills = auth()->user()->computerSkills()->get();
        return view('computer-skills.index', compact('competences', 'computerSkills', 'userComputerSkills'));
    }

    public function store(Request $request)
    {
        DB::table('computer_skill_user')->insert(
            [
                'user_id' => auth()->id(),
                'computer_skill_id' => $request->input('computer_skill_id'),
                'competence_id' => $request->input('competence_id')
            ]
        );


        $progress = auth()->user()->progress()->first();
        $progress->computer_skills = true;
        $progress->save();         

        flash('Success!')->success();

        return redirect('computer-skills');
    }

    public function destroy($id)
    {
        $computerSkill = auth()->user()->computerSkills()->findOrFail($id);

        auth()->user()->computerSkills()->detach($computerSkill);

        flash('Computer skill deleted successfully!')->success();

        return redirect('computer-skills');
    }

}
