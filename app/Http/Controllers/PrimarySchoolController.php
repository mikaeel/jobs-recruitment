<?php

namespace App\Http\Controllers;

use App\PrimarySchool;
use Illuminate\Http\Request;

class PrimarySchoolController extends Controller
{
    public function store(Request $request)
    {
    	// Validation
    	
    	
    	PrimarySchool::updateOrCreate(
    		['user_id' => auth()->id()],
            [
        		'name' => $request->input('name'),
        		'completed' => $request->input('completed'),
        		'region' => $request->input('region'),
        		'district' => $request->input('district'),
                'certificate' => $request->hasFile('certificate') ? $path = $request->certificate->store('uploads') : '',
            ]
    	);

    	flash('Success');

    	return back();
    }
}
