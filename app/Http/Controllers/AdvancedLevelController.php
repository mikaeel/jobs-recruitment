<?php

namespace App\Http\Controllers;

use App\AdvancedLevel;
use Illuminate\Http\Request;

class AdvancedLevelController extends Controller
{
	public function store(Request $request)
	{
    	// validation
    	
    	AdvancedLevel::updateOrCreate(
    		['user_id' => auth()->id()],
            [
    		'index_number' => $request->input('index_number'),
    		'certificate' => $request->hasFile('certificate') ? $path = $request->certificate->store('uploads') : '',
            ]
    	);         

    	flash('Success');

    	return back();		
	}
}
