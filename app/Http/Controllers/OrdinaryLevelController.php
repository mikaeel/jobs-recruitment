<?php

namespace App\Http\Controllers;

use App\OrdinaryLevel;
use App\ApplicantProgress;
use Illuminate\Http\Request;

class OrdinaryLevelController extends Controller
{
    public function store(Request $request)
    {
    	// validation   
    	
    	OrdinaryLevel::updateOrCreate(
    		['user_id' => auth()->id()],
    		[
            'index_number' => $request->input('index_number'),
    		'certificate' => $request->hasFile('certificate') ? $path = $request->certificate->store('uploads') : '',
            ]
    	);   

        $progress = auth()->user()->progress()->first();
        $progress->education_background  = true;
        $progress->save();             

    	flash('Success');

    	return back();
    }
}
