<?php

namespace App\Http\Controllers;

use App\Interest;
use App\User;
use Illuminate\Http\Request;

class InterestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $interests = auth()->user()->interests()->get();
        return view('interests.index', compact('interests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $interest = new Interest();
        $interest->name = $request->input('name');

        auth()->user()->interests()->save($interest);

        $progress = auth()->user()->progress()->first();
        $progress->interests = true;
        $progress->save();        

        flash('Success')->success();

        return redirect('interests');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Interest $interest
     * @return \Illuminate\Http\Response
     */
    public function show(Interest $interest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Interest $interest
     * @return \Illuminate\Http\Response
     */
    public function edit(Interest $interest)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Interest $interest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Interest $interest)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        
        $interest->name = $request->input('name');
        $interest->save();

        flash('Interest updated successfully')->success();

        return redirect('interests');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Interest $interest
     * @return \Illuminate\Http\Response
     */
    public function destroy(Interest $interest)
    {
        $interest->delete();

        flash('Interest deleted successfully')->success();

        return redirect('interests');
    }
}
