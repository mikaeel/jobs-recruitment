<?php

namespace App\Http\Controllers;

use App\AdditionalSkill;
use Illuminate\Http\Request;

class AdditionalSkillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $additionalSkills = auth()->user()->additionalSkills()->get();
        return view('additional-skills.index', compact('additionalSkills'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
                'name' => 'required'
            ]);

        $additionalSkill = new AdditionalSkill();
        $additionalSkill->name = $request->input('name');

        auth()->user()->additionalSkills()->save($additionalSkill);

        $progress = auth()->user()->progress()->first();
        $progress->additional_skills = true;
        $progress->save();         

        flash('Success')->success();

        return redirect('additional-skills');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AdditionalSkill $additionalSkill
     * @return \Illuminate\Http\Response
     */
    public function show(AdditionalSkill $additionalSkill)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AdditionalSkill $additionalSkill
     * @return \Illuminate\Http\Response
     */
    public function edit(AdditionalSkill $additionalSkill)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\AdditionalSkill $additionalSkill
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdditionalSkill $additionalSkill)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdditionalSkill $additionalSkill
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdditionalSkill $additionalSkill)
    {
        //
    }
}
