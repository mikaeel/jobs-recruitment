<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrdinaryLevel extends Model
{
    protected $fillable = [
    	'user_id',
    	'index_number',
    	'certificate',
    ];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
