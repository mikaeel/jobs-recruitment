<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Qualification extends Model
{
    protected $fillable = ['name', 'description'];

    public function higherEducation()
    {
    	return $this->hasMany(HigherEducation::class);
    }
}
