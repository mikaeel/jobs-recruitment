<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class EducationBackground extends Model
{
    protected $dates = [
        'starts_at', 'ends_at',
    ];

    public function setStartsAtAttribute($value)
    {
        $this->attributes['starts_at'] = Carbon::parse($value);
    }

    public function setEndsAtAttribute($value)
    {
        $this->attributes['ends_at'] = Carbon::parse($value);
    }

    public function users()
    {
        return $this->belongsTo(User::class);
    }

    public function educationLevel()
    {
        return $this->belongsTo(EducationLevel::class);
    }

    public function educationArea()
    {
        return $this->belongsTo(EducationArea::class);
    }
}
