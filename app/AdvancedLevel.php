<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvancedLevel extends Model
{
    protected $fillable = [
    	'user_id',
    	'index_number',
    	'certificate',
    ];
}
