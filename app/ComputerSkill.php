<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComputerSkill extends Model
{
    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot('competence_id');
    }
}
