@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2">
            @include('partials.sidebar')
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Interests</div>
                <div class="panel-body">
                    @include('errors.list')
                    <div class="row">
                        <div class="col-sm-8">
                            @if($interests->count())
                            <table class="table table-bordered table-responsive">
                                <tbody>
                                    @php $i = 1 @endphp
                                    @foreach($interests as $interest)
                                    <tr>
                                        <td class="text-right">{{ $i++ }}.</td>
                                        <td>{{ $interest->name }}</td>
                                        <td>
                                            <a class="btn btn-primary btn-sm" data-toggle="modal"
                                            href='#{{ $interest->id }}'>
                                            Edit</a>
                                            <div class="modal fade" id="{{ $interest->id }}">
                                                <div class="modal-dialog">
                                                    <form method="POST"
                                                    action="{{ route('interests.update', $interest->id) }}">
                                                    {{ csrf_field() }}
                                                    {{ method_field('PATCH') }}
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close"
                                                            data-dismiss="modal" aria-hidden="true">
                                                            &times;
                                                        </button>
                                                        <h4 class="modal-title">
                                                            Edit {{ $interest->name }}</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <input type="text" name="name" id="name"
                                                                value="{{ $interest->name }}"
                                                                class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default"
                                                            data-dismiss="modal">Cancel
                                                        </button>
                                                        <button type="submit" class="btn btn-primary">
                                                            Update
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <a class="btn btn-danger btn-sm" data-toggle="modal"
                                    href='#delete{{ $interest->id }}'>Delete</a>
                                    <div class="modal fade" id="delete{{ $interest->id }}">
                                        <div class="modal-dialog">
                                            <form method="POST"
                                            action="{{ route('interests.destroy', $interest->id) }}">
                                            {{ csrf_field() }}
                                            {{ method_field("DELETE") }}
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close"
                                                    data-dismiss="modal" aria-hidden="true">
                                                    &times;
                                                </button>
                                                <h4 class="modal-title">
                                                    Delete {{ $interest->name }}</h4>
                                                </div>
                                                <div class="modal-body">
                                                    Are you sure that you want to permanently delete
                                                    <strong>{{ $interest->name }}</strong>?
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default"
                                                    data-dismiss="modal">Cancel
                                                </button>
                                                <button type="submit" class="btn btn-danger">
                                                    Delete
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @else
            <div class="info">
                <p class="lead">
                    You have 0 interests
                </p>
            </div>
            @endif
        </div>
        <div class="col-sm-4">
            <form action="{{ route('interests.store') }}" method="POST">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="name">Add Interest</label>
                    <input type="text" name="name" id="name" class="form-control">
                </div>

                <button type="submit" class="btn btn-primary">Add</button>
            </form>
        </div>
    </div>
</div>
</div>
</div>
<div class="col-sm-2">
    
    @include('partials.applicant.progress', ['user' => Auth::user()])
    
</div> 
</div>
</div>
@endsection
