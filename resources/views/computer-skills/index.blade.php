@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">Computer Skills</div>

                    <div class="panel-body">

                        @if($userComputerSkills->count())
                            <table class="table table-bordered table-responsive">
                                <thead>
                                <tr>
                                    <th class="text-right">SN</th>
                                    <th>Skill</th>
                                    <th>Competence</th>
                                    <th>Added</th>
                                    <th>Delete</th>
                                    <th>Remove</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $i = 1 @endphp
                                @foreach($userComputerSkills as $userComputerSkill)
                                    <tr>
                                        <td class="text-right">{{ $i++ }}.</td>
                                        <td>
                                            {{ $userComputerSkill->name }}
                                        </td>
                                        <td>
                                            @foreach($competences as $competence)
                                                @if($userComputerSkill->pivot->competence_id == $competence->id)
                                                    {{ $competence->name }}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            {{ $userComputerSkill->created_at->diffForHumans() }}
                                        </td>
                                        <td>
                                            Edit
                                        </td>
                                        <td>
                                            <a class="btn btn-danger btn-sm" data-toggle="modal"
                                               href='#delete{{ $userComputerSkill->id }}'>Delete</a>
                                            <div class="modal fade" id="delete{{ $userComputerSkill->id }}">
                                                <div class="modal-dialog">
                                                    <form method="POST"
                                                          action="{{ route('computer-skills.destroy', $userComputerSkill->id) }}">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-hidden="true">&times;
                                                                </button>
                                                                <h4 class="modal-title">
                                                                    Delete {{ $userComputerSkill->name }}</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                Delete <strong>{{ $userComputerSkill->name }}</strong>?
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default"
                                                                        data-dismiss="modal">Cancel
                                                                </button>
                                                                <button type="submit" class="btn btn-primary">Delete
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="info">
                                <p class="lead">
                                    You have 0 computer skills.
                                </p>
                            </div>
                        @endif

                    </div>
                    <div class="panel-footer">
                        <a class="btn btn-primary" data-toggle="modal" href='#modal-id'>Add Computer Skill</a>
                        <div class="modal fade" id="modal-id">
                            <div class="modal-dialog">
                                <form action="{{ route('computer-skills.store') }}" method="POST">
                                    {{ csrf_field() }}

                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                &times;
                                            </button>
                                            <h4 class="modal-title">Add Computer Skill</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="computer_skill_id">Skill</label>
                                                <select name="computer_skill_id" id="computer_skill_id"
                                                        class="form-control" required="required">
                                                    <option value="">-Select-</option>
                                                    @foreach($computerSkills as $computerSkill)
                                                        <option value="{{ $computerSkill->id }}">{{ $computerSkill->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Competence</label>
                                                <br>
                                                @foreach($competences as $competence)
                                                    <label class="radio-inline">
                                                        <input type="radio" name="competence_id" id="competence_id"
                                                               value="{{ $competence->id }}">
                                                        {{ $competence->name }}
                                                    </label>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel
                                            </button>
                                            <button type="submit" class="btn btn-primary">Add</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                
                @include('partials.applicant.progress', ['user' => Auth::user()])
                
            </div> 
        </div>
    </div>
@endsection
