@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">Professional Trainings</div>

                    <div class="panel-body">
                        @if($professionalTrainings->count())
                            @foreach($professionalTrainings->chunk(3) as $professionalTrainingSet)
                                <div class="row">
                                    @foreach($professionalTrainingSet as $professionalTraining)
                                        <div class="col-sm-4">
                                            <div class="thumbnail">
                                                <img data-src="#" alt="">
                                                <div class="caption">
                                                    <h3>{{ $professionalTraining->name }}</h3>
                                                    <p>
                                                        {{ $professionalTraining->training_date->toFormattedDateString() }}
                                                    </p>
                                                    <p>
                                                        <a href="#" class="btn btn-primary btn-sm">Edit</a>
                                                        <a href="#" class="btn btn-danger btn-sm">Delete</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                        @else
                            <div class="alert alert-info">You have not added any professional training yet.</div>
                        @endif
                    </div>
                    <div class="panel-footer">
                        <a href="{{ route('professional-training.create') }}" class="btn btn-primary">Add New</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                
                @include('partials.applicant.progress', ['user' => Auth::user()])
                
            </div> 
        </div>
    </div>
@endsection
