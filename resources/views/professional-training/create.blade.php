@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2">
            @include('partials.sidebar')
        </div>
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">Add Professional Trainings</div>
                <div class="panel-body">
                    @include('errors.list')
                    <form action="{{ route('professional-training.store') }}" method="POST" class="form-horizontal" role="form">
                        {{ csrf_field() }}

                        @include('professional-training._form')

                        <div class="form-group">
                            <div class="col-sm-10 col-sm-offset-2">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
