<div class="form-group">
    <label for="name" class="col-sm-2 control-label">Name of the Training</label>
    <div class="col-sm-5">
        <input type="text" name="name" id="name" class="form-control">
    </div>
</div>

<div class="form-group">
    <label for="description" class="col-sm-2 control-label">About the Training (Option)</label>
    <div class="col-sm-5">
        <textarea name="description" id="description" rows="5" class="form-control"></textarea>
    </div>
</div>

<div class="form-group">
    <label for="training_date" class="col-sm-2 control-label">Date</label>
    <div class="col-sm-5">
        <input type="date" name="training_date" id="training_date" class="form-control">
    </div>
</div>
