@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">Additional Skills</div>
                    <div class="panel-body">
                    @include('errors.list')
                        <div class="row">
                            <div class="col-sm-8">
                                <ul>
                                    @foreach($additionalSkills as $additionalSkill)
                                        <li>{{ $additionalSkill->name }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-sm-4">
                                <form action="{{ route('additional-skills.store') }}" method="POST">
                                    {{ csrf_field() }}

                                    <div class="form-group">
                                        <label for="name">Additional Skill</label>
                                        <input type="text" name="name" id="name" class="form-control">
                                    </div>

                                    <button type="submit" class="btn btn-primary">Add</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                
                @include('partials.applicant.progress', ['user' => Auth::user()])
                
            </div> 
        </div>
    </div>
@endsection
