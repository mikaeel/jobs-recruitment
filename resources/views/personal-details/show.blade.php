@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-sm-8">
                @component('components.panel')

                @slot('header')
                Personal Details
                @endslot

                @slot('body')

                @include('errors.list')

                <form action="/personal-details" method="POST" role="form">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="first_name">First name</label>
                                <input type="text" name="first_name" id="first_name" value="{{ $user->first_name or old('first_name') }}" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="middle_name">Middle name</label>
                                <input type="text" name="middle_name" id="middle_name"
                                   value="{{ $user->middle_name or old('middle_name') }}" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="last_name">Last name</label>
                                <input type="text" name="last_name" id="last_name"
                                   value="{{ $user->last_name or old('last_name') }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="dob">Date of Birth</label>
                                <input type="date" name="dob" id="dob"
                                   value="{{ isset($user->dob) ? $user->dob : old('dob') }}"
                                   class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                            <label for="gender">Gender</label>
                            <label>
                                <input type="radio" name="gender" id="gender"
                                       value="Male" {{ $user->gender == 'Male' ? 'checked' : '' }} required>
                                Male
                            </label>                              
                            <label>
                                <input type="radio" name="gender" id="gender"
                                       value="Female" {{ $user->gender == 'Female' ? 'checked' : '' }} required>
                                Female
                            </label>                                
                            </div>                                                      
                        </div> 
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="nationality">Nationality</label>
                                <select name="nationality" id="nationality" class="form-control" required>
                                    <option value="">-Select-</option>
                                    @foreach($nationalities as $nationality)
                                        <option value="{{ $nationality->name }}"
                                        @if($user->nationality === $nationality->name)
                                            selected="selected" 
                                        @endif
                                        >{{ $nationality->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>                       
                    </div>

                    <h5>Contact Info</h5>

                    <div class="row">
                        <div class="col-sm-6">
                            <label for="phone">Phone</label>
                            <input type="tel" name="phone" id="phone" value="{{ $user->phone or old('phone') }}"
                                   class="form-control" required>
                        </div>
                        <div class="col-sm-6">
                            <label for="email">Email</label>
                             <input type="email" name="email" id="email" value="{{ $user->email or old('email') }}"
                                   class="form-control" required disabled>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <label for="place_of_domicile">Place of Domicile</label>
                            <input type="text" name="place_of_domicile" id="place_of_domicile"
                                   value="{{ $user->place_of_domicile or old('place_of_domicile') }}"
                                   class="form-control" required>
                        </div>
                        <div class="col-sm-6">
                            <label for="current_location">Current Location</label>
                            <input type="text" name="current_location" id="current_location"
                                   value="{{ $user->current_location or old('current_location') }}"
                                   class="form-control" required>
                        </div>
                    </div>
                        
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="address">Address</label>
                            <textarea name="address" id="address" class="form-control"
                                      rows="2" required>{{ $user->address or old('address') }}</textarea>                            
                        </div>                        
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
        
                </form>

                @endslot

                @endcomponent
            </div>
            <div class="col-sm-2">
                
                @include('partials.applicant.progress', ['user' => Auth::user()])
                
            </div>            
        </div>
    </div>
@endsection
