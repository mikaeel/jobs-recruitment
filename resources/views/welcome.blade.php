@extends('layouts.app')

@section('title', 'Home')

@section('content')

   <div class="container">
       <div class="row">
           <div class="col-sm-8">
               <h3>Welcome to UDOM Recruitment portal</h3>

                <h4>Instructions</h4>

                <p>To apply for a job, you must complete the following</p>

                <h5>Step 1: Register</h5>
                <h5>Step 2: Verify your email address</h5>
                <h5>Step 3: Login</h5>
                <h5>Step 4: Complete your profile</h5>
                <h5>Step 5: Apply to a vacancy</h5>
                <ol>
                    <li>Click on the <strong>Available Jobs</strong> link on the top</li>
                    <li>Select job vacancy of your interest</li>
                    <li>Read the job requirements thoroughly</li>
                    <li>Click <strong>Apply</strong> to apply for that job vacancy</li>
                </ol>

                <h5>Important</h5>
                <ul>
                    <li>Make sure that all the information you fill are correct</li>
                    <li>If you are experiencing any problems please give us a feedback <a href="{{ route('support') }}">here</a></li>
                </ul>               
               
           </div>
           <div class="col-sm-4">
               <h4>Latest job vacancies</h4>
               @if($jobs->count())
                @foreach($jobs as $job)
                 <div class="media">
                     <div class="media-body">
                         <h4 class="media-heading">{{ $job->title }}</h4>
                         <p>
                          <a href="{{ route('jobs.show', $job->id) }}">More &raquo;</a>
                          </p>
                     </div>
                 </div>
                 @endforeach
               @endif                  
           </div>
       </div>
   </div>

@endsection
