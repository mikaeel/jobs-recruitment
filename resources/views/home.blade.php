@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-sm-8">
                <div class="panel panel-default">
                    <div class="panel-heading">Welcome to UDOM Recruitment portal</div>

                    <div class="panel-body">
                        <h5>
                            Please Complete your Profile to maximize your earning opportunities
                        </h5>
                        <p>
                            A professional profile is essential to effectively showcase your skills. Applicants who have completed their profile are far more likely to get hired
                        </p>
                        <h5>Step 1: Complete your profile</h5>
                        <p>
                            Please complete ALL of the required fields of the forms found in the left-hand menu of this page. 
                        </p>
                        <h5>Step 2: Apply to a vacancy</h5>
                        <ol>
                            <li>Click on the <strong>Browse Vacancies</strong> link on the top</li>
                            <li>Select job vacancy of your interest</li>
                            <li>Read the job requirements thoroughly</li>
                            <li>Click <strong>Apply</strong> to apply for that job vacancy</li>
                        </ol>

                        <h5>Important</h5>
                        <ul>
                            <li>Make sure that all the information you fill are correct</li>
                            <li>If you are experiencing any problems please give us a feedback <a href="#">here</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                
                @include('partials.applicant.progress', ['user' => Auth::user()])
                
            </div>
        </div>
    </div>
@endsection
