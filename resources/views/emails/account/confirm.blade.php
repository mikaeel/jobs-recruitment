@component('mail::message')
# Hello

Thanks for registering

We just need you to confirm your email address.

@component('mail::button', ['url' => url('register/confirm/' . $user->token)])
Confirm Email Address
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
