@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit Reference</div>

                    <div class="panel-body">
                        <form action="{{ route('references.update', $reference->id) }}" method="POST"
                              class="form-horizontal"
                              role="form">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}

                            @include('references._form')

                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
