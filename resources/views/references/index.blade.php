@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2">
            @include('partials.sidebar')
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">My References</div>

                <div class="panel-body">
                    @if($references->count())
                    @foreach($references->chunk(3) as $referenceSet)
                    <div class="row">
                        @foreach($referenceSet as $reference)
                        <div class="col-sm-4">
                            <div class="thumbnail">
                                <div class="caption">
                                    <h3>
                                        {{ $reference->title }}.
                                        {{ $reference->first_name }}
                                        {{ $reference->middle_name }}
                                        {{ $reference->last_name }}
                                    </h3>
                                    <p>
                                        {{ $reference->institution }}
                                    </p>
                                    <p>
                                        Phone: {{ $reference->phone }}
                                    </p>
                                    <p>
                                        Email: {{ $reference->email }}
                                    </p>
                                    <p>
                                        <a class="btn btn-primary btn-sm"
                                        href="{{ route('references.edit', $reference->id) }}">Edit</a>
                                        <a class="btn btn-danger btn-sm" data-toggle="modal"
                                        href='#modal-id'>Delete</a>
                                        <div class="modal fade" id="modal-id">
                                            <div class="modal-dialog">
                                                <form method="POST"
                                                action="{{ route('references.destroy', $reference->id) }}">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}

                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close"
                                                        data-dismiss="modal" aria-hidden="true">
                                                        &times;
                                                    </button>
                                                    <h4 class="modal-title">Delete Reference</h4>
                                                </div>
                                                <div class="modal-body">
                                                    Are you sure that you want to permanently delete
                                                    <strong>
                                                        {{ $reference->title }}.
                                                        {{ $reference->first_name }}
                                                        {{ $reference->middle_name }}
                                                        {{ $reference->last_name }}
                                                    </strong>?
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default"
                                                    data-dismiss="modal">Cancel
                                                </button>
                                                <button type="submit" class="btn btn-danger">
                                                    Delete
                                                </button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @endforeach
        @else
        <div class="info">
            <p class="lead">
                You have 0 references
            </p>
        </div>
        @endif
    </div>
    <div class="panel-footer">
        <a href="{{ route('references.create') }}" class="btn btn-primary btn-sm">Add New</a>
    </div>
</div>
</div>
<div class="col-sm-2">
    
    @include('partials.applicant.progress', ['user' => Auth::user()])
    
</div> 
</div>
</div>
@endsection
