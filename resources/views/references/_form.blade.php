<div class="form-group">
    <label for="title" class="col-sm-3 control-label">Title</label>
    <div class="col-sm-5">
        <select name="title" id="title" class="form-control">
            <option value="">-Select-</option>
            <option value="Mr">Mr</option>
            <option value="Mrs">Mrs</option>
            <option value="Miss">Miss</option>
            <option value="Dr">Dr</option>
            <option value="Prof">Prof</option>
        </select>
    </div>
</div>

<div class="form-group">
    <label for="first_name" class="col-sm-3 control-label">First Name</label>
    <div class="col-sm-5">
        <input type="text"
               name="first_name"
               id="first_name"
               value="{{ $reference->first_name or old('first_name') }}"
               class="form-control"
               placeholder="First Name"
               required="required">
    </div>
</div>

<div class="form-group">
    <label for="middle_name" class="col-sm-3 control-label">Middle Name (Option)</label>
    <div class="col-sm-5">
        <input type="text"
               name="middle_name"
               id="middle_name"
               value="{{ $reference->middle_name or old('middle_name') }}"
               class="form-control"
               placeholder="Middle Name (Option)"
               required="required">
    </div>
</div>

<div class="form-group">
    <label for="last_name" class="col-sm-3 control-label">Last Name</label>
    <div class="col-sm-5">
        <input type="text"
               name="last_name"
               id="last_name"
               value="{{ $reference->last_name or old('last_name') }}"
               class="form-control"
               placeholder="Last Name"
               required="required">
    </div>
</div>

<div class="form-group">
    <label for="phone" class="col-sm-3 control-label">Phone</label>
    <div class="col-sm-5">
        <input type="tel"
               name="phone"
               id="phone"
               value="{{ $reference->phone or old('phone') }}"
               class="form-control"
               placeholder="+XXX XXX XXX XXX">
    </div>
</div>

<div class="form-group">
    <label for="phone" class="col-sm-3 control-label">Email</label>
    <div class="col-sm-5">
        <input type="email"
               name="email"
               id="email"
               value="{{ $reference->email or old('email') }}"
               class="form-control"
               placeholder="Email Address">
    </div>
</div>

<div class="form-group">
    <label for="institution" class="col-sm-3 control-label">Institution</label>
    <div class="col-sm-5">
        <input type="text"
               name="institution"
               id="institution"
               value="{{ $reference->institution or old('institution') }}"
               class="form-control"
               placeholder="Institution name">
    </div>
</div>

<div class="form-group">
    <label for="address" class="col-sm-3 control-label">Address</label>
    <div class="col-sm-5">
        <input type="text" name="address" id="address" value="{{ $reference->address or old('address') }}" class="form-control">
    </div>
</div>
