<div class="col-sm-12">
<div class="row">
    <div class="col-lg-12 text-center">
        <h3>
            CURRICULLUM VITAE FOR {{ strtoupper($user->first_name) }} {{ strtoupper($user->middle_name) }} {{ strtoupper($user->last_name) }}
        </h3>
    </div>	
</div>
<div class="row">
    <h4>Personal Information</h4>
    <table class="table table-bordered">
        <tbody>
            <tr>
                <td>Surname</td>
                <td>{{ $user->last_name }}</td>
            </tr>
            <tr>
                <td>First name</td>
                <td>{{ $user->first_name }}</td>
            </tr>
            <tr>
                <td>Middle name</td>
                <td>{{ $user->middle_name }}</td>
            </tr>
            <tr>
                <td>Gender</td>
                <td>{{ $user->gender }}</td>
            </tr>
            <tr>
                <td>Address</td>
                <td>{{ $user->address }}</td>
            </tr>
            <tr>
                <td>Phone</td>
                <td>{{ $user->phone }}</td>
            </tr>
            <tr>
                <td>Email</td>
                <td>{{ $user->email }}</td>
            </tr>
            <tr>
                <td>Date of birth</td>
                <td>{{ $user->dob }}</td>
            </tr>
            <tr>
                <td>Nationality</td>
                <td>{{ $user->nationality }}</td>
            </tr>
            <tr>
                <td>Languages</td>
                <td>{{ $user->language_proficiency }}</td>
            </tr>
        </tbody>
    </table>
    <h4>Education</h4>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Date</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            @isset($user->advancedLevel)
                <tr>
                    <th>Advanced Level</th>
                    <td>Index number: {{ $user->advancedLevel->index_number }}</td>
                </tr>
            @endisset
            @isset($user->ordinaryLevel)               
            <tr>
                <th>Ordinary Level</th>
                <td>Index number: {{ $user->ordinaryLevel->index_number }}</td>
            </tr>
            @endisset
            @isset($user->primarySchool)
                <tr>
                    <th>Primary School</th>
                    <td>
                        {{ $user->primarySchool->name }}. 
                        {{ $user->primarySchool->region }},
                        {{ $user->primarySchool->district }}
                    </td>
                </tr>
            @endisset
        </tbody>
    </table>

    <h4>Employment History</h4>

    @if($user->employmentHistories->count())
        <ul>
            @foreach($user->employmentHistories as $employmentHistory)
                <li>
                    {{ $employmentHistory->company_name }} <br>
                    <strong>Address</strong>
                    {{ $employmentHistory->address }} 
                    <strong>Phone:</strong> {{ $employmentHistory->phone }} 
                    <strong>Job Title:</strong> {{ $employmentHistory->job_title }} 
                    <strong>Job Responsibilities: </strong>
                    <br> {{ $employmentHistory->job_responsibilities }} <br>
                    <strong>From</strong> {{ $employmentHistory->starts_at }}
                    <strong>To</strong> {{ $employmentHistory->ends_at }}
                </li>
            @endforeach
        </ul>
    @else
        No Employment History
    @endif            

    <h4>Professional Training</h4>

    @if($user->professionalTrainings->count())
        <ul>
            @foreach($user->professionalTrainings as $professionalTraining)
                <li>{{ $professionalTraining->name }}
                    - {{ $professionalTraining->training_date->toFormattedDateString() }}</li>
            @endforeach
        </ul>
    @else
        No Professional training
    @endif            

    <h4>Computer Skills</h4>

    @if($user->computerSkills->count())
        <ul>
            @foreach($user->computerSkills as $computerSkill)
                <li>
                    {{ $computerSkill->name }}
                </li>
            @endforeach
        </ul>
    @else
        No computer skills
    @endif            

    <h3>Interests</h3>

    @if($user->interests->count())
        <ul>
            @foreach($user->interests as $interest)
                <li>{{ $interest->name }}</li>
            @endforeach
        </ul>
    @else
        No interests
    @endif            

    <h4>Additional Skills</h4>

    @if($user->additionalSkills->count())
        <ul>
            @foreach($user->additionalSkills as $additionalSkill)
                <li>{{ $additionalSkill->name }}</li>
            @endforeach
        </ul>
    @else
        No Additional Skills
    @endif

    <h4>References</h4>
    @if(Auth()->user()->references->count())
        <ul>
            @foreach($user->references as $reference)
                <li>
                    {{ $reference->title }}
                    . {{ $reference->first_name }} {{ $reference->middle_name }} {{ $reference->last_name }}

                    <br>
                    <strong>Institution:</strong> {{ $reference->institution }} <br>
                    <strong>Phone:</strong> {{ $reference->phone }} <br>
                    <strong>Email:</strong> {{ $reference->email }}
                </li>
            @endforeach
        </ul>
    @else
        No references
    @endif

    <h4>Attached Documents</h4>

    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Document</th>
                    <th>Preview</th>
                    <th>Download</th>
                </tr>
            </thead>
            <tbody>
                {{-- Advanced Level --}}
                @isset($user->advancedLevel)
                    @isset($user->advancedLevel->certificate)
                        <tr>
                            <td>{{ $user->advancedLevel->id }}.</td>
                            <td>ACSEE</td>
                            <td>
                                <form action="{{ route('preview') }}" method="POST">
                                    {{ csrf_field() }}

                                    <input type="hidden" name="filepath" value="{{ $user->advancedLevel->certificate }}">

                                    <button type="submit" class="btn btn-primary btn-sm">Preview</button>
                                </form>
                            </td>                            
                            <td>
                                <form action="{{ route('download') }}" method="POST">
                                    {{ csrf_field() }}

                                    <input type="hidden" name="filepath" value="{{ $user->advancedLevel->certificate }}">

                                    <button type="submit" class="btn btn-primary btn-sm">Download</button>
                                </form>
                            </td>
                        </tr>
                    @endisset
                @endisset                        
                {{-- O Level --}}
                @isset($user->ordinaryLevel)
                    @isset($user->ordinaryLevel->certificate)
                        <tr>
                            <td>{{ $user->ordinaryLevel->id }}.</td>
                            <td>CSEE</td>
                            <td>
                                <form action="{{ route('preview') }}" method="POST">
                                    {{ csrf_field() }}

                                    <input type="hidden" name="filepath" value="{{ $user->ordinaryLevel->certificate }}">

                                    <button type="submit" class="btn btn-primary btn-sm">Preview</button>
                                </form>
                            </td>                            
                            <td>
                                <form action="{{ route('download') }}" method="POST">
                                    {{ csrf_field() }}

                                    <input type="hidden" name="filepath" value="{{ $user->ordinaryLevel->certificate }}">

                                    <button type="submit" class="btn btn-primary btn-sm">Download</button>
                                </form>
                            </td>
                        </tr>
                    @endisset
                @endisset 
                {{-- Primary School --}}
                @isset($user->primarySchool)
                    @isset($user->primarySchool->certificate)
                        <tr>
                            <td>{{ $user->primarySchool->id }}.</td>
                            <td>Certificate of Primary School</td>
                            <td>
                                <form action="{{ route('preview') }}" method="POST">
                                    {{ csrf_field() }}

                                    <input type="hidden" name="filepath" value="{{ $user->primarySchool->certificate }}">

                                    <button type="submit" class="btn btn-primary btn-sm">Preview</button>
                                </form>
                            </td>                            
                            <td>
                                <form action="{{ route('download') }}" method="POST">
                                    {{ csrf_field() }}

                                    <input type="hidden" name="filepath" value="{{ $user->primarySchool->certificate }}">

                                    <button type="submit" class="btn btn-primary btn-sm">Download</button>
                                </form>
                            </td>
                        </tr>
                    @endisset
                @endisset                                 
            </tbody>
        </table>
    </div>
</div>	
</div>