<div class="container footer-top">
	<div class="row">
		<div class="col-sm-4">
			<h3><span class="lnr lnr-phone-handset"></span> Contact</h3>

			<br>
			<strong>University of Dodoma</strong><br>
			P.O.BOX 259 Dodoma <br>
			Tanzania, East Africa

			<br><br>
			Telephone:
			<br><br>
			Email: info@udom-ors.ac.tz
		</div>
		<div class="col-sm-4">
			<h3><span class="lnr lnr-link"></span> Important Links</h3>
			<ul class="list-unstyled">
				<li><a href="http://udom.ac.tz">
					<span class="lnr lnr-unlink"></span> University of Dodoma (UDOM)
				</a></li>
			</ul>
		</div>
		<div class="col-sm-4">
			<h3><span class="lnr lnr-question-circle"></span> Support</h3>
			<p>
				For any enquiries please call <strong>(000) 000 000</strong>
			</p>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row footer-bottom">
		<p class="lead text-center">&copy; {{ date('Y') }} {{ Config('app.name') }} . All Rights Reserved</p>
	</div>	
</div>