<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Your Progresses</h3>
    </div>
    <div class="panel-body">   

        {{ $progress = (($user->totalProgress() / 8) * 100) }}%

        <div class="progress">
        <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="{{ $progress }}" aria-valuemin="{{ $progress }}" aria-valuemax="100" style="width: {{ $progress }}%;">
            <span class="sr-only">{{ $progress }}</span>
        </div>
    </div>  

    <h4>Important details to complete first</h4>
    <div class="checkbox">
        <label>
            <input type="checkbox"
            @if($user->progress()->first()->personal_details)
            checked
            @endif
            disabled>
            Personal details
        </label>
    </div> 
    
    <div class="checkbox">
        <label>
            <input type="checkbox"
            @if($user->progress()->first()->education_background)
            checked
            @endif
            disabled>
            Education background
        </label>
    </div>  

    <div class="checkbox">
        <label>
            <input type="checkbox"
            @if($user->progress()->first()->interests)
            checked
            @endif
            disabled>
            Interests
        </label>
    </div>

    <div class="checkbox">
        <label>
            <input type="checkbox"
            @if($user->progress()->first()->references)
            checked
            @endif
            disabled>
            References
        </label>
    </div>         


    <h4>Other details</h4>

    <div class="checkbox">
        <label>
            <input type="checkbox"
            @if($user->progress()->first()->employment_history)
            checked
            @endif
            disabled>
            Employment history
        </label>
    </div>

    <div class="checkbox">
        <label>
            <input type="checkbox"
            @if($user->progress()->first()->professional_training)
            checked
            @endif
            disabled>
            Professional Training
        </label>
    </div>

    <div class="checkbox">
        <label>
            <input type="checkbox"
            @if($user->progress()->first()->additional_skills)
            checked
            @endif
            disabled>
            Additional skills
        </label>
    </div>

    <div class="checkbox">
        <label>
            <input type="checkbox"
            @if($user->progress()->first()->computer_skills)
            checked
            @endif
            disabled>
            Computer skills
        </label>
    </div>

</div>
</div>