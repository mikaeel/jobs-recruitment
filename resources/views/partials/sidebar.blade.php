<div class="list-group">
    <a href="/home" class="list-group-item {{ url()->current() === url('home') ? 'active' : '' }}">
        <span class="lnr lnr-home"></span> Home
    </a>
    <a href="/personal-details" class="list-group-item {{ url()->current() === url('personal-details') ? 'active' : '' }}">
        <span class="lnr lnr-user"></span> Personal Details
    </a>
    <a href="{{ route('education-background.create') }}"
       class="list-group-item {{ url()->current() === url('education-background/create') ? 'active' : '' }}">
        <span class="lnr lnr-pencil"></span> Education
    </a>
    <a href="/higher-education"
       class="list-group-item {{ url()->current() === url('higher-education') ? 'active' : '' }}">
        <span class="lnr lnr-pencil"></span> Higher Education
    </a>
    <a href="/employment-history" class="list-group-item {{ url()->current() === url('employment-history') ? 'active' : '' }}">
        <span class="lnr lnr-apartment"></span> Employment
    </a>
    <a href="{{ route('professional-training.index') }}" class="list-group-item {{ url()->current() === url('professional-training') ? 'active' : '' }}">
        <span class="lnr lnr-paperclip"></span> Professional Training
    </a>
    <a href="{{ route('additional-skills.index') }}" class="list-group-item {{ url()->current() === url('additional-skills') ? 'active' : '' }}">
        <span class="lnr lnr-coffee-cup"></span> Additional Skills
    </a>
    <a href="{{ route('computer-skills.index') }}" class="list-group-item {{ url()->current() === url('computer-skills') ? 'active' : '' }}">
        <span class="lnr lnr-laptop-phone"></span> Computer Skills
    </a>
    <a href="{{ route('interests.index') }}" class="list-group-item {{ url()->current() === url('interests') ? 'active' : '' }}">
        <span class="lnr lnr-heart-pulse"></span> Interests
    </a>
    <a href="{{ route('references.index') }}" class="list-group-item {{ url()->current() === url('references') ? 'active' : '' }}">
        <span class="lnr lnr-users"></span> References
    </a>
    <a href="{{ route('my-cv') }}" class="list-group-item {{ url()->current() === url('my-cv') ? 'active' : '' }}">
        <span class="lnr lnr-license"></span> My CV
    </a>
</div>

<div class="list-group">
    <a href="{{ route('my-applications') }}" class="list-group-item {{ url()->current() === url('my-applications') ? 'active' : '' }}"><span class="lnr lnr-pencil"></span> My Applications</a>
</div>