<ol class="breadcrumb">
    <li class="{{ url()->current() === url('home') ? 'active' : '' }}">
        <a href="{{ route('home') }}">Home</a>
    </li>
    @if(Request::segment(1))
	    <li class="active">
	    	{{ Request::segment(1) }}
	    </li>
    @endif
</ol>