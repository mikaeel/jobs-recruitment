<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                <li class="{{ url()->current() === url('/') ? 'active' : '' }}">
                    <a href="/">Home</a>
                </li>
                <li class="{{ url()->current() === url(route('about')) ? 'active' : '' }}">
                    <a href="{{ route('about') }}">About</a>
                </li>
                <li class="{{ url()->current() === url(route('contact')) ? 'active' : '' }}">
                    <a href="{{ route('contact') }}">Contact</a>
                </li>
                <li class="{{ url()->current() === url(route('jobs.index')) ? 'active' : '' }}">
                    <a href="{{ route('jobs.index') }}">Available Jobs</a>
                </li>
                <li class="{{ url()->current() === url(route('support')) ? 'active' : '' }}">
                    <a href="{{ route('support') }}">Support</a>
                </li>
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li class="{{ url()->current() === url(route('login')) ? 'active' : '' }}"><a href="{{ route('login') }}">Login</a></li>
                    <li class="{{ url()->current() === url(route('register')) ? 'active' : '' }}"><a href="{{ route('register') }}">Register</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false">
                            {{ Auth::user()->email }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('home') }}">Home</a></li>
                            @if(Auth::check())
                                @role('human-resource')
                                    <li>
                                        <a href="{{ route('dashboard') }}"><span class="lnr lnr-home"></span> Dashboard</a>
                                    </li>
                                @endrole
                            @endif
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <span class="lnr lnr-exit"></span> Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>