@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit {{ $employmentHistory->job_title }}</div>

                    <div class="panel-body">

                        <form action="{{ route('employment-history.edit', $employmentHistory->id) }}" method="POST"
                              class="form-horizontal" role="form">

                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}

                            @include('employment-history._form')


                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <button type="submit" class="btn btn-primary btn-lg">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
