@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Employment History</h3>
                    </div>

                    <div class="panel-body">
                        @if($employmentHistories->count())
                            <table class="table table-bordered table-hover table-responsive">
                                <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Organization</th>
                                    <th>Title</th>
                                    <th>Responsibilities</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach($employmentHistories as $employmentHistory)
                                    <tr>
                                        <td>{{ $i++ }}.</td>
                                        <td>{{ $employmentHistory->company_name }}</td>
                                        <td>{{ $employmentHistory->job_title }}</td>
                                        <td>{{ $employmentHistory->job_responsibilities }}</td>
                                        <td>
                                            <a href="{{ route('employment-history.edit', $employmentHistory->id) }}">Edit</a>
                                        </td>
                                        <td>
                                            <a href="#">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            No Employment History
                        @endif
                    </div>

                    <div class="panel-footer">
                        <a href="{{ route('employment-history.create') }}" class="btn btn-primary">Add</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                
                @include('partials.applicant.progress', ['user' => Auth::user()])
                
            </div> 
        </div>
    </div>
@endsection
