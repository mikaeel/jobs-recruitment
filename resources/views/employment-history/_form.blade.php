<div class="form-group">
    <label for="company_name" class="col-sm-2 control-label">Name of the Organization</label>
    <div class="col-sm-10">
        <input type="text" name="company_name" id="company_name"
               value="{{ $employmentHistory->company_name or old('company_name') }}" class="form-control">
    </div>
</div>

<div class="form-group">
    <label for="address" class="col-sm-2 control-label">Address</label>
    <div class="col-sm-10">
        <input type="text" name="address" id="address" value="{{ $employmentHistory->address or old('address') }}" class="form-control">
    </div>
</div>

<div class="form-group">
    <label for="phone" class="col-sm-2 control-label">Phone</label>
    <div class="col-sm-10">
        <input type="text" name="phone" id="phone" value="{{ $employmentHistory->phone or old('phone') }}" class="form-control">
    </div>
</div>

<div class="form-group">
    <label for="job_title" class="col-sm-2 control-label">Job Title</label>
    <div class="col-sm-10">
        <input type="text" name="job_title" id="job_title" class="form-control">
    </div>
</div>

<div class="form-group">
    <label for="job_responsibilities" class="col-sm-2 control-label">Job Responsibilities</label>
    <div class="col-sm-10">
        <textarea name="job_responsibilities" id="job_responsibilities" rows="5" class="form-control"></textarea>
    </div>
</div>

<div class="form-group">
    <label for="starts_at" class="col-sm-2 control-label">From</label>
    <div class="col-sm-10">
        <input type="text" name="starts_at" id="starts_at" class="form-control">
    </div>
</div>

<div class="form-group">
    <label for="ends_at" class="col-sm-2 control-label">To</label>
    <div class="col-sm-10">
        <input type="text" name="ends_at" id="ends_at" class="form-control">
    </div>
</div>  