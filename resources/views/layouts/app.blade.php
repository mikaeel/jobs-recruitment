<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') {{ config('app.name', 'UDOM-RS') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link href="/css/app.css" rel="stylesheet">
</head>
<body>
    <div id="app">

        @include('partials.header')

        @include('partials.navbar')

        @if(Auth::check())
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    @include('partials.breadcrumb')
                </div>
            </div>
        </div>
        @endif

        <div class="container-fluid">
            @include('flash::message')
        </div>

        @yield('content')

        <footer>
            <div class="well">
                @include('partials.footer')
            </div>
        </footer>

    </div>

    <!-- Scripts -->
    <script src="/js/app.js"></script>
    <script>
        $('#flash-overlay-modal').modal();
    </script>
</body>
</html>
