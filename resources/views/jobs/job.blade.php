<div class="thumbnail">
    <div class="caption">
        <h3>
            <a href="{{ route('jobs.show', $job->id) }}">
                {{ $job->title }}
            </a>
        </h3>
        <p>
            <span class="label label-warning">{{ $job->jobCategory->name }}</span>
        </p>
        <p>
            {!! str_limit($job->body, 300) !!}
            <a href="{{ route('jobs.show', $job->id) }}">More &raquo;</a>
        </p>
        <p>
            <small>Posted: {{ $job->created_at->toDateString() }}</small>
        </p>
        <p>
            <small>Deadline: {{ $job->closed_at->toDateString() }}</small>
        </p>
    </div>
</div>