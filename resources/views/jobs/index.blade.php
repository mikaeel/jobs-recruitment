@extends('layouts.app')

@section('title', 'Job Vacancies')

@section('content')

    <div class="container">
        <div class="page-header">
            <h1>
                Job Vacancies
            </h1>
        </div>
        <div class="row">
            <div class="col-sm-8">
                @if($jobs->count())
                    @foreach($jobs as $job)
                        <div class="col-sm-12">
                            @include('jobs.job')
                        </div>
                    @endforeach

                    {{ $jobs->links() }}
                @else
                    <div class="alert alert-warning">
                        Currently there are no job vacancies
                    </div>
                @endif                
            </div>
            <div class="col-sm-4">
                <div class="list-group">
                    <h3>Category</h3>
                    @foreach($jobCategories as $jobCategory)
                       <a href="{{ route('job-category.show', $jobCategory->slug) }}" class="list-group-item">
                           {{ $jobCategory->name }}
                       </a>
                   @endforeach
               </div>                               
            </div>
        </div>
    </div>

@endsection
