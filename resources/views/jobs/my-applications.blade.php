@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('partials.sidebar')
            </div>
            <div class="col-sm-8">
               <div class="panel panel-default">
                   <div class="panel-heading">
                       <h3 class="panel-title">My Applications</h3>
                   </div>
                   <div class="panel-body">
                    @if($jobs->count())
                        @foreach($jobs as $job)
                            <div class="col-sm-12">
                                @include('jobs.job')
                            </div>
                        @endforeach
                    @else
                        <div class="alert alert-warning">
                            No job application
                        </div>

                        <div class="text-center">
                          <a href="/jobs">Available Jobs</a>
                        </div>
                    @endif                        
                   </div>
               </div>
            </div>
            <div class="col-sm-2">
                
                @include('partials.applicant.progress', ['user' => Auth::user()])
                
            </div> 
        </div>
    </div>
@endsection
