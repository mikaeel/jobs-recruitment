@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="page-header">
            <h1>
                {{ $job->title }} ({{ $job->positions }} Positions)
            </h1>
        </div>
        <div class="row">
            <div class="col-sm-8">
                {!! $job->body !!}

                <hr>

                <a class="btn btn-primary btn-lg" data-toggle="modal" href='#modal-id'>Apply</a>
                <div class="modal fade" id="modal-id">
                    <div class="modal-dialog">
                        <form method="POST" action="{{ route('job.apply', [$job->slug, $job->id]) }}">
                            {{ csrf_field() }}

                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        &times;
                                    </button>
                                    <h4 class="modal-title">Confirm applying for {{ $job->title }}</h4>
                                </div>
                                <div class="modal-body">
                                    Continue applying for <strong>{{ $job->title }}</strong>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">No
                                    </button>
                                    <button type="submit" class="btn btn-primary">Yes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>


            </div>
            <div class="col-sm-4">
                <div class="well">
                    <h3>Details</h3>
                    
                    <p><strong>Category:</strong> {{ $job->jobCategory->name }}</p>

                    <p><strong>Posted</strong> {{ $job->created_at->toFormattedDateString() }}</p>

                    <p><strong>Deadline: </strong> {{ $job->closed_at->toFormattedDateString() }}</p>
                </div>
            </div>
        </div>
    </div>

@endsection
