@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('dashboard.partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">New Job Vacancy</div>

                    <div class="panel-body">
                        @include('errors.list')
                        <form action="{{ route('dashboard.jobs.store') }}" method="POST" role="form">
                                
                            {{ csrf_field() }}

                            @include('dashboard.jobs._form')   
                                

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Post</button>
                                </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
