@extends('layouts.app')

@section('title', $job->title)

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2">
            @include('dashboard.partials.sidebar')
        </div>
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">{{ $job->title }}</div>

                <div class="panel-body">
                    <h2>{{ $job->title }} Applicants</h2>

                    <div class="pull-right">
                        <form action="{{ route('reports.jobs.applicants', $job->id) }}" method="post">
                            {{ csrf_field() }}

                            <button type="submit" class="btn btn-primary">Export All</button>
                        </form> 
                        <br><br>                          
                    </div>

                    @if($job->applicants()->count())
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="text-right">SN</th>
                                <th>Name</th>
                                <th>Gender</th>
                                <th>F4 Index number</th>
                                <th>F6 Index number</th>
                                <th>Date of Application</th>
                                <th>Called for Interview</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $i = 1 @endphp
                            @foreach($applicants as $applicant)
                            <tr>
                                <td class="text-right">{{ $i++ }}.</td>
                                <td>
                                    <a href="{{ route('dashboard.jobs.show.applicants.show', [$job->slug,$job->id,$applicant->id]) }}">
                                        {{ $applicant->first_name }}
                                        {{ $applicant->middle_name }}
                                        {{ $applicant->last_name }}
                                    </a>
                                </td>
                                <td>{{ $applicant->gender }}</td>
                                <td>{{ $applicant->ordinaryLevel->index_number }}</td>
                                <td>
                                    @if($applicant->advancedLevel)
                                        {{ $applicant->advancedLevel->index_number }}
                                    @endif
                                </td>
                                <td>{{ $applicant->pivot->created_at->toFormattedDateString() }}</td>
                                <td>
                                    @if($applicant->pivot->called_for_interview)
                                        <label class="label label-primary">Yes</label>
                                    @else
                                        @include('dashboard.partials.actions.call_for_interview')
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                    <p class="lead">
                        {{ $job->applicants()->count() }} applicants
                    </p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
