@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('dashboard.partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit {{ $job->title }}</div>

                    <div class="panel-body">
                        @include('errors.list')
                        <form action="{{ route('dashboard.jobs.update', $job->id) }}" method="POST" role="form">
                                
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}

                            @include('dashboard.jobs._form')   
                                

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
