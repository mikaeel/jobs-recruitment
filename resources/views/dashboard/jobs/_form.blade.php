<div class="form-group">
    <label for="title">Title</label>
    <input type="text" class="form-control" name="title" id="title" value="{{ $job->title or old('title') }}" required>
</div>
<div class="form-group">
    <label for="positions">Positions</label>
    <input type="number" class="form-control" name="positions" id="title" value="{{ $job->positions or old('positions') }}" required>
    <p class="help-text text-muted">
    	Please specify the number of positions for this job
    </p>
</div>

<div class="form-group">
	<label for="job_category_id">Category</label>
	<select name="job_category_id" id="job_category_id" class="form-control" required>
		<option value="">-Select-</option>
		@foreach($jobCategories as $jobCategory)
			<option value="{{ $jobCategory->id }}">{{ $jobCategory->name }}</option>
		@endforeach
	</select>
</div>

<div class="form-group">
    <label for="body">Description</label>
    <textarea class="form-control" name="body" id="body" rows="10" required>{{ $job->body or old('body') }}</textarea>
</div>

<div class="form-group">
    <label for="closed_at">Deadline of Application</label>
    <input type="date" class="form-control" name="closed_at" id="closed_at" required>
</div>
