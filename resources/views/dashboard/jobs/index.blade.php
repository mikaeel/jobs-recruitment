@extends('layouts.app')

@section('title', 'Jobs')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <a href="{{ route('dashboard.jobs.create') }}" class="btn btn-primary btn-sm">New</a> <br> <br>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                @include('dashboard.partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">Jobs</div>

                    <div class="panel-body">
                        @if($jobs->count())
                            <table class="table table-bordered table-hover table-responsive">
                                <thead>
                                <tr>
                                    <th class="text-right">SN</th>
                                    <th>Title</th>
                                    <th>Elligible Applicants</th>
                                    <th>Deadline</th>
                                    <th>Posted</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $i = 1 @endphp
                                @foreach($jobs as $job)
                                    <tr>
                                        <td class="text-right">{{ $i++ }}.</td>
                                        <td>
                                            <a href="{{ route('dashboard.jobs.show', [$job->slug, $job->id]) }}">
                                                {{ $job->title }}
                                            </a>
                                        </td>
                                        <td>
                                            <span class="badge">
                                                {{ $job->applicants_count }}
                                            </span>
                                        </td>
                                        <td>{{ $job->closed_at }}</td>
                                        <td>{{ $job->created_at }}</td>
                                        <td>
                                            <a href="{{ route('dashboard.jobs.edit', $job->id) }}">Edit</a>
                                        </td>
                                        <td>
                                            <a href="#">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <p class="lead">
                                No Job Posted
                            </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
