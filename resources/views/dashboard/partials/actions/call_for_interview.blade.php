<a class="btn btn-primary" data-toggle="modal" href='#call-{{ $applicant->id }}'>Call for Interview</a>
<div class="modal fade" id="call-{{ $applicant->id }}">
    <div class="modal-dialog">
        <form method="POST"
              action="{{ route('dashboard.interviews.store', [$job->id, $applicant->id]) }}">
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;
                    </button>
                    <h4 class="modal-title">Confirm
                        calling {{ $applicant->first_name }} {{ $applicant->middle_name }} {{ $applicant->last_name }}
                        for interview</h4>
                </div>
                <div class="modal-body">
                    Call {{ $applicant->first_name }} {{ $applicant->middle_name }} {{ $applicant->last_name }}
                    for interview
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Cancel
                    </button>
                    <button class="btn btn-primary">Call</button>
                </div>
            </div>
        </form>
    </div>
</div>