<div class="list-group">
    <a href="{{ route('dashboard') }}" class="list-group-item {{ Request::is('dashboard') ? 'active' : '' }}">
        <span class="lnr lnr-home"></span> Dashboard
    </a>
    <a href="/dashboard/jobs" class="list-group-item {{ Request::is('dashboard/jobs') ? 'active' : '' }}">
        <span class="lnr lnr-briefcase"></span> Jobs
    </a>  
    <a href="{{ route('dashboard.applicants.index') }}" class="list-group-item {{ Request::is('dashboard/applicants') ? 'active' : '' }}">
        <span class="lnr lnr-users"></span> Applicants
    </a>
    <a href="#" class="list-group-item">Users</a>     
</div>

<div class="list-group">
    <a href="#" class="list-group-item">Settings</a>
    <a href="{{ route('job-categories.index') }}" class="list-group-item {{ url()->current() === url('settings/job-categories') ? 'active' : '' }}">Job Categories</a>
    <a href="{{ route('settings.computer-skills.index') }}" class="list-group-item {{ url()->current() === url('settings/computer-skills') ? 'active' : '' }}">Computer Skills</a>
    <a href="{{ route('settings.competences.index') }}" class="list-group-item {{ url()->current() === url('settings/competences') ? 'active' : '' }}">Competences</a>
    {{-- <a href="{{ route('settings.education-levels.index') }}" class="list-group-item {{ url()->current() === url('settings/education-levels') ? 'active' : '' }}">Education Levels</a> --}}
    {{-- <a href="{{ route('settings.education-areas.index') }}" class="list-group-item {{ url()->current() === url('settings/education-areas') ? 'active' : '' }}">Education Areas</a> --}}
    <a href="/dashboard/qualification-level" class="list-group-item {{ Request::is('dashboard/qualification-level') ? 'active' : '' }}">
        <span class="lnr lnr-briefcase"></span> Qualification Level
    </a>
    <a href="{{ route('settings.nationalities.index') }}" class="list-group-item {{ url()->current() === url('settings/nationalities') ? 'active' : '' }}">Nationalities</a>
</div>
