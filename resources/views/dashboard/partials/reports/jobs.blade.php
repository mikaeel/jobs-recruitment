@php $i = 1 @endphp
@foreach($jobs as $job)
    <tr>
        <td>{{ $i++ }}.</td>
        <td colspan="10">{{ $job->title }}</td>
    </tr>
    @if($job->applicants()->count())
    <tr>
        <th></th>
        <th>SN</th>
        <th>First name</th>
        <th>Midde name</th>
        <th>Last name</th>
        <th>Gender</th>
        <th>Date of birth</th>
        <th>Phone</th>
        <th>Email</th>
        <th>F4 Index number</th>
        <th>F6 Index number</th>
    </tr>
        @php $j = 1 @endphp
        @foreach($job->applicants as $applicant)
            <tr>
                <td>&nbsp;</td>
                <td>{{ $j++ }}.</td>
                <td>{{ $applicant->first_name }}</td>
                <td>{{ $applicant->middle_name }}</td>
                <td>{{ $applicant->last_name }}</td>
                <td>{{ $applicant->gender }}</td>
                <td>{{ $applicant->dob }}</td>
                <td>{{ $applicant->phone }}</td>
                <td>{{ $applicant->email }}</td>
                <td>
                    @if(null !== $applicant->ordinaryLevel)
                        {{ $applicant->ordinaryLevel->index_number }}
                    @else
                        -
                    @endif
                </td>
                <td>
                    @if(null !== $applicant->advancedLevel)
                        {{ $applicant->advancedLevel->index_number }}
                    @else
                        -
                    @endif
                </td>
            </tr>
        @endforeach
        <tr><td colspan="11">&nbsp;</td></tr>
    @endif
@endforeach