<div class="form-group">
	<label for="name" class="col-sm-3 control-label">Name of the Qualification Level</label>
	<div class="col-sm-5">
		<input type="text" name="name" id="name" class="form-control" value="{{ isset($qualification->name) ? $qualification->name : old('name') }}">
	</div>
</div>

<div class="form-group">
	<label for="description" class="col-sm-3 control-label">About the Qualification Level (Option)</label>
	<div class="col-sm-5">
		<textarea name="description" id="description" rows="5" class="form-control">{{ isset($qualification->description) ? $qualification->description : old('description') }}</textarea>
	</div>
</div>
