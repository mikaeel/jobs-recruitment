@extends('layouts.app')

@section('title', 'Update Qualification Level')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2">
            @include('dashboard.partials.sidebar')
        </div>
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">Update Qualification Level</div>

                <div class="panel-body">
                    @include('errors.list')
                    <form action="/dashboard/qualification-level/{{$qualification->id}}" method="POST" class="form-horizontal" role="form">
                        {{ csrf_field() }}
                        {{ method_field('PATCH')}}

                        @include('dashboard.qualifications._form')

                        <div class="form-group">
                            <div class="col-sm-10 col-sm-offset-2">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
