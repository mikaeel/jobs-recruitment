@extends('layouts.app')

@section('title', 'Add Qualification Level')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2">
            @include('dashboard.partials.sidebar')
        </div>
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">Add Qualification Level</div>

                <div class="panel-body">
                    @include('errors.list')
                    <form action="/dashboard/qualification-level" method="POST" class="form-horizontal" role="form">
                        {{ csrf_field() }}

                        @include('dashboard.qualifications._form')

                        <div class="form-group">
                            <div class="col-sm-6 col-sm-offset-3">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
