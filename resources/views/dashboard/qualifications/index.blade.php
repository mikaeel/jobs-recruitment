@extends('layouts.app')

@section('title', 'Qualification Level')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <a href="/dashboard/qualification-level/create" class="btn btn-primary btn-sm">New Qualification Level</a> <br> <br>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            @include('dashboard.partials.sidebar')
        </div>
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">Qualification Level  <span class="badge">
                    {{ $qualifications->count() }}
                </span></div>

                <div class="panel-body">
                    @if($qualifications->count())
                    <table class="table table-bordered table-hover table-responsive">
                        <thead>
                            <tr>
                                <th class="text-right">SN</th>
                                <th>Qualification Name</th>
                                <th>Qualification Descreption</th>
                                <th>Created Date</th>
                                <th>Posted Date</th>
                                <th colspan="2">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $i = 1 @endphp
                            @foreach($qualifications as $qualification)
                            <tr>
                                <td class="text-right">{{ $i++ }}.</td>
                                <td>
                                    <a href="#">
                                        {{ $qualification->name }}
                                    </a>
                                </td>
                                <td>{{ $qualification->description}}</td>
                                <td>{{ $qualification->created_at->diffForHumans() }}</td>
                                <td>{{ $qualification->updated_at->diffForHumans() }}</td>
                                <td>
                                    <a href="/dashboard/qualification-level/{{$qualification->id}}/edit" class="btn btn-success btn-sm">Edit</a>
                                </td>
                                <td>
                                    <form action="/dashboard/qualification-level/{{$qualification->id}}" method="POST" role="form">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE')}}
                                        <a class="btn btn-danger btn-sm" data-toggle="modal" href='#modal-id'>Delete</a>
                                        <div class="modal fade" id="modal-id">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h4 class="modal-title">{{ $qualification->name }}</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        Are you sure you want to delete {{ $qualification->name }} ?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                        <button type="submit" class="btn btn-danger">Delete</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                    <p class="lead">
                        No Qualification Level Posted
                    </p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
