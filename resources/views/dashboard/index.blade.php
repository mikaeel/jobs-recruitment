@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('dashboard.partials.sidebar')
            </div>
            <div class="col-md-10">

                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#applicants" aria-controls="applicants" role="tab" data-toggle="tab">All Applicants</a>
                        </li>
                        <li role="presentation">
                            <a href="#called-for-interview" aria-controls="called-for-interview" role="tab" data-toggle="tab">Called for Interview</a>
                        </li>
                        <li role="presentation">
                            <a href="#not-called-for-interview" aria-controls="not-called-for-interview" role="tab" data-toggle="tab">Not Called for Interview</a>
                        </li>
                    </ul>
                
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="applicants">

                            <div class="panel">
                                <div class="panel-body">
                                    @if($jobs->count())
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-responsive">
                                                <tbody>
                                                    <tr>
                                                        <td colspan="10"></td>
                                                        <td><a href="{{ route('reports.jobs-with-applicants') }}" class="btn btn-primary">Export</a></td>
                                                    </tr>
                                                    @include('dashboard.partials.reports.jobs')
                                                </tbody>
                                            </table>
                                        </div>
                                    @else
                                        <p class="text-center">No Data</p>
                                    @endif
                                </div>
                            </div>

                        </div>
                        <div role="tabpanel" class="tab-pane" id="called-for-interview">
                            
                            <div class="panel">
                                <div class="panel-body">
                                    @if($jobsWithApplicantsCalledForInterview->count())
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-responsive">
                                                <tbody>
                                                    <tr>
                                                        <td colspan="10"></td>
                                                        <td><a href="{{ route('reports.jobs-with-applicants-call-for-interview') }}" class="btn btn-primary">Export</a></td>
                                                    </tr>
                                                    @include('dashboard.partials.reports.jobs', ['jobs' => $jobsWithApplicantsCalledForInterview])
                                                </tbody>
                                            </table>
                                        </div>
                                    @else
                                        <p class="text-center">No Data</p>
                                    @endif
                                </div>
                            </div>

                        </div>
                        <div role="tabpanel" class="tab-pane" id="not-called-for-interview">
                            
                            <div class="panel">
                                <div class="panel-body">
                                    @if($jobsWithApplicantsNotCalledForInterview->count())
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-responsive">
                                                <tbody>
                                                    <tr>
                                                        <td colspan="10"></td>
                                                        <td><a href="{{ route('reports.jobs-with-applicants-not-call-for-interview') }}" class="btn btn-primary">Export</a></td>
                                                    </tr>
                                                    @include('dashboard.partials.reports.jobs', ['jobs' => $jobsWithApplicantsNotCalledForInterview])
                                                </tbody>
                                            </table>
                                        </div>
                                    @else
                                        <p class="text-center">No Data</p>
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
