@extends('layouts.app')

@section('title', $applicant->first_name .' '. $applicant->middle_name .' '. $applicant->last_name)

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('dashboard.partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ $applicant->first_name .' '. $applicant->middle_name .' '. $applicant->last_name }}</div>

                    <div class="panel-body">
                        @include('partials.cv', ['user' => $applicant])
                    </div>
                    @empty($job)
                        <div class="alert alert-warning">
                            <strong>Note!</strong> This person has not applied for any job
                        </div>
                    @endempty
                </div>
            </div>
        </div>
    </div>
@endsection
