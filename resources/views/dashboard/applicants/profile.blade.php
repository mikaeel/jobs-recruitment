@extends('layouts.app')

@section('title', $applicant->first_name .' '. $applicant->middle_name .' '. $applicant->last_name)

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('dashboard.partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">Profile of {{ $applicant->first_name .' '. $applicant->middle_name .' '. $applicant->last_name }}</div>

                    <div class="panel-body">
                        
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Jobs Applied ({{ $applicant->job_applications()->count() }})</h3>
                    </div>
                    <div class="panel-body">
                        
                        @if($applicant->job_applications()->count())
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>Title</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $i = 1 @endphp
                                        @foreach($applicant->job_applications as $job)
                                            <tr>
                                                <td>{{ $i++ }}.</td>
                                                <td>{{ $job->title }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <p class="text-center">No job applied</p>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
