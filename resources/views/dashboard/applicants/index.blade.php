@extends('layouts.app')

@section('title', 'Job Applicants')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('dashboard.partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">Job Applicants</div>

                    <div class="panel-body">
                        @if($applicants->count())
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered table-responsive">
                                    <thead>
                                        <tr>
                                            <th class="text-right">SN</th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Gender</th>
                                            <th>Jobs Applied</th>
                                            <th>Registered</th>
                                            <th>Profile</th>
                                            <th>CV</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $i=1 @endphp
                                        @foreach($applicants as $applicant)
                                        <tr>
                                            <td class="text-right">{{ $i++ }}.</td>
                                            <td>
                                                  {{ $applicant->first_name }}
                                                  {{ $applicant->middle_name }}
                                                  {{ $applicant->last_name }}
                                            </td>
                                            <td>{{ $applicant->phone }}</td>
                                            <td>{{ $applicant->email }}</td>
                                            <td>{{ $applicant->gender }}</td>
                                            <td>{{ $applicant->job_applications_count }}</td>
                                            <td>{{ $applicant->created_at->toFormattedDateString() }}</td>
                                            <td>
                                                <a href="{{ route('dashboard.applicants.profile', $applicant->id) }}">
                                                    Click to view
                                                </a>
                                            </td>
                                            <td>
                                                <a href="{{ route('dashboard.applicants.show', [$applicant->id]) }}">
                                                    Click to view
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <p class="lead">
                                {{ $applicants->count() }} applicants
                            </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
