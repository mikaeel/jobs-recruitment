<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Curriculum Vitae</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h3>
                    CURRICULLUM VITAE FOR {{ strtoupper(Auth()->user()->first_name) }} {{ strtoupper(Auth()->user()->middle_name) }} {{ strtoupper(Auth()->user()->last_name) }}
                </h3>
            </div>
        </div>
        <div class="row">
            <h4>Personal Information</h4>
            <table class="table table-bordered table-responsive">
                <tbody>
                    <tr>
                        <td>Surname</td>
                        <td>{{ Auth::user()->last_name }}</td>
                    </tr>
                    <tr>
                        <td>First name</td>
                        <td>{{ Auth::user()->first_name }}</td>
                    </tr>
                    <tr>
                        <td>Middle name</td>
                        <td>{{ Auth::user()->middle_name }}</td>
                    </tr>
                    <tr>
                        <td>Gender</td>
                        <td>{{ Auth::user()->gender }}</td>
                    </tr>
                    <tr>
                        <td>Address</td>
                        <td>{{ Auth::user()->address }}</td>
                    </tr>
                    <tr>
                        <td>Phone</td>
                        <td>{{ Auth::user()->phone }}</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>{{ Auth::user()->email }}</td>
                    </tr>
                    <tr>
                        <td>Date of birth</td>
                        <td>{{ Auth::user()->dob }}</td>
                    </tr>
                    <tr>
                        <td>Nationality</td>
                        <td>{{ Auth::user()->nationality }}</td>
                    </tr>
                    <tr>
                        <td>Languages</td>
                        <td>{{ Auth::user()->language_proficiency }}</td>
                    </tr>
                </tbody>
            </table>
            <h4>Education</h4>
            <table class="table table-bordered table-responsive">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    @isset(Auth::user()->advancedLevel)
                        <tr>
                            <td>&nbsp;</td>
                            <td>Index number: {{ Auth::user()->advancedLevel->index_number }}</td>
                        </tr>
                    @endisset                
                    <tr>
                        <td>&nbsp;</td>
                        <td>Index number: {{ Auth::user()->ordinaryLevel->index_number }}</td>
                    </tr>
                    @isset(Auth::user()->primarySchool)
                        <tr>
                            <td>{{ Auth::user()->primarySchool->completed }}</td>
                            <td>
                                {{ Auth::user()->primarySchool->name }}. 
                                {{ Auth::user()->primarySchool->region }},
                                {{ Auth::user()->primarySchool->district }}
                            </td>
                        </tr>
                    @endisset
                </tbody>
            </table>

            <h4>Employment History</h4>

            @if(Auth::user()->employmentHistories->count())
                <ul>
                    @foreach($user->employmentHistories as $employmentHistory)
                        <li>
                            {{ $employmentHistory->company_name }} <br>
                            <strong>Address</strong>
                            {{ $employmentHistory->address }} 
                            <strong>Phone:</strong> {{ $employmentHistory->phone }} 
                            <strong>Job Title:</strong> {{ $employmentHistory->job_title }} 
                            <strong>Job Responsibilities: </strong>
                            <br> {{ $employmentHistory->job_responsibilities }} <br>
                            <strong>From</strong> {{ $employmentHistory->starts_at }}
                            <strong>To</strong> {{ $employmentHistory->ends_at }}
                        </li>
                    @endforeach
                </ul>
            @else
                No Employment History
            @endif            

            <h4>Professional Training</h4>

            @if(Auth::user()->professionalTrainings->count())
                <ul>
                    @foreach($user->professionalTrainings as $professionalTraining)
                        <li>{{ $professionalTraining->name }}
                            - {{ $professionalTraining->training_date->toFormattedDateString() }}</li>
                    @endforeach
                </ul>
            @else
                No Professional training
            @endif            

            <h4>Computer Skills</h4>

            @if(Auth::user()->computerSkills->count())
                <ul>
                    @foreach($user->computerSkills as $computerSkill)
                        <li>
                            {{ $computerSkill->name }}
                        </li>
                    @endforeach
                </ul>
            @else
                No computer skills
            @endif            

            <h3>Interests</h3>

            @if(Auth::user()->interests->count())
                <ul>
                    @foreach($user->interests as $interest)
                        <li>{{ $interest->name }}</li>
                    @endforeach
                </ul>
            @else
                No interests
            @endif            

            <h4>Additional Skills</h4>

            @if(Auth::user()->additionalSkills->count())
                <ul>
                    @foreach($user->additionalSkills as $additionalSkill)
                        <li>{{ $additionalSkill->name }}</li>
                    @endforeach
                </ul>
            @else
                No Additional Skills
            @endif

            <h4>References</h4>
            @if(Auth()->user()->references->count())
                <ul>
                    @foreach($user->references as $reference)
                        <li>
                            {{ $reference->title }}
                            . {{ $reference->first_name }} {{ $reference->middle_name }} {{ $reference->last_name }}

                            <br>
                            <strong>Institution:</strong> {{ $reference->institution }} <br>
                            <strong>Phone:</strong> {{ $reference->phone }} <br>
                            <strong>Email:</strong> {{ $reference->email }}
                        </li>
                    @endforeach
                </ul>
            @else
                No references
            @endif
        </div>
    </div>
</body>
</html>