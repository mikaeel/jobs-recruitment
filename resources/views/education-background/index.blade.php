@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">Education Background</div>

                    <div class="panel-body">

                        @if($educationBackgrounds->count())
                            <table class="table table-bordered table-hover table-responsive">
                                <thead>
                                <tr>
                                    <th class="text-right">SN</th>
                                    <th>Level</th>
                                    <th>Area</th>
                                    <th>From</th>
                                    <th>To</th>
                                    <th>Edit</th>
                                    <th>Remove</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $i = 1 @endphp
                                @foreach($educationBackgrounds as $educationBackground)
                                    <tr>
                                        <td class="text-right">{{ $i++ }}.</td>
                                        <td>{{ $educationBackground->educationLevel->name }}</td>
                                        <td>{{ $educationBackground->educationArea->name }}</td>
                                        <td>{{ $educationBackground->starts_at->toFormattedDateString() }}</td>
                                        <td>{{ $educationBackground->ends_at->toFormattedDateString() }}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info">
                                You have not added any education background yet
                            </div>
                        @endif

                        <a href="{{ route('education-background.create') }}">Add New</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                
                @include('partials.applicant.progress', ['user' => Auth::user()])
                
            </div>            
        </div>
    </div>
@endsection
