@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-sm-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">Primary School</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <form action="{{ route('primary-school.store') }}" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="name">School Name</label>
                                        <input type="text" name="name" id="name" value="{{ isset(Auth::user()->primarySchool) ? Auth::user()->primarySchool->name : '' }}" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="completed">Year Completed</label>
                                        <input type="number" name="completed" id="completed" value="{{ isset(Auth::user()->primarySchool) ? Auth::user()->primarySchool->completed : '' }}" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="region">Region</label>
                                        <input type="text" name="region" id="region" value="{{ isset(Auth::user()->primarySchool) ? Auth::user()->primarySchool->region : '' }}" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="district">District</label>
                                        <input type="text" name="district" id="district" value="{{ isset(Auth::user()->primarySchool) ? Auth::user()->primarySchool->district : '' }}" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="certificate">Certificate (Option)</label>
                                        <input type="file" name="certificate">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">Ordinary Level</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <form action="{{ route('ordinary-level.store') }}" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="index_number">F4 Index Number</label>
                                        <input type="text" name="index_number" id="index_number" value="{{ isset(Auth()->user()->ordinaryLevel) ? Auth()->user()->ordinaryLevel->index_number : '' }}" class="form-control" required>
                                        <p class="help-block">
                                            Format: S0155-0049-2010
                                        </p>
                                    </div>
                                    <div class="form-group">
                                        <label for="certificate">Certificate</label>
                                        <input type="file" name="certificate" {{ isset(Auth::user()->ordinaryLevel->certificate) ? '' : 'required' }}>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                                </form>                                
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">Advanced Level</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <form action="{{ route('advanced-level.store') }}" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="index_number">F6 Index Number</label>
                                        <input type="text" name="index_number" id="index_number" value="{{ isset(Auth::user()->advancedLevel) ? Auth::user()->advancedLevel->index_number : '' }}" class="form-control" required>
                                        <p class="help-block">
                                            Format: S0155-0590-2013
                                        </p>
                                    </div>
                                    <div class="form-group">
                                        <label for="certificate">Certificate</label>
                                        <input type="file" name="certificate" {{ isset(Auth::user()->advancedLevel->certificate) ? '' : 'required' }}>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </div>                                    
                                </form>
                            </div>
                        </div>
                    </div>
                </div>                               
            </div>
            <div class="col-sm-2">
                
                @include('partials.applicant.progress', ['user' => Auth::user()])
                
            </div> 
        </div>
    </div>
@endsection
