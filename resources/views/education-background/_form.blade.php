<div class="form-group">
    <label for="education_level_id" class="col-sm-2 control-label">Education Level</label>
    <div class="col-sm-10">
        <select name="education_level_id" id="education_level_id" class="form-control">
            <option value="">-Select-</option>
            @foreach($educationLevels as $educationLevel)
                <option value="{{ $educationLevel->id }}">{{ $educationLevel->name }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group">
    <label for="education_area_id" class="col-sm-2 control-label">Education Area</label>
    <div class="col-sm-10">
        <select name="education_area_id" id="education_area_id" class="form-control">
            <option value="">-Select-</option>
            @foreach($educationAreas as $educationArea)
                <option value="{{ $educationArea->id }}">{{ $educationArea->name }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group">
    <label for="starts_at" class="col-sm-2 control-label">From</label>
    <div class="col-sm-10">
        <input type="date" name="starts_at" id="starts_at" class="form-control">
    </div>
</div>

<div class="form-group">
    <label for="ends_at" class="col-sm-2 control-label">To</label>
    <div class="col-sm-10">
        <input type="date" name="ends_at" id="ends_at" class="form-control">
    </div>
</div>

<div class="form-group">
    <label for="notes" class="col-sm-2 control-label">Notes (Option)</label>
    <div class="col-sm-10">
        <textarea name="notes" id="notes" rows="5" class="form-control"></textarea>
    </div>
</div>