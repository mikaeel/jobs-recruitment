<div class="form-group">
	<label for="registration_number" class="col-sm-3 control-label">Registration Number</label>
	<div class="col-sm-5">
		<input type="text" name="registration_number" id="registration_number" class="form-control" value="{{ isset($higherEducation->registration_number) ? $higherEducation->registration_number : old('registration_number') }}">
	</div>
</div>
<div class="form-group">
	<label for="program_name" class="col-sm-3 control-label">Program Name</label>
	<div class="col-sm-5">
		<input type="text" name="program_name" id="program_name" class="form-control" value="{{ isset($higherEducation->program_name) ? $higherEducation->program_name : old('program_name') }}">
	</div>
</div>
<div class="form-group">
	<label for="graduation_year" class="col-sm-3 control-label">Year of Graduation</label>
	<div class="col-sm-5">
		<input type="text" name="graduation_year" id="graduation_year" class="form-control" value="{{ isset($higherEducation->graduation_year) ? $higherEducation->graduation_year : old('graduation_year') }}">
	</div>
</div>
<div class="form-group">
	<label for="overall_gpa" class="col-sm-3 control-label">Over all GPA</label>
	<div class="col-sm-5">
		<input type="text" name="overall_gpa" id="overall_gpa" class="form-control" value="{{ isset($higherEducation->overall_gpa) ? $higherEducation->overall_gpa : old('overall_gpa') }}">
	</div>
</div>
<div class="form-group">
	<label for="certificate" class="col-sm-3 control-label">Upload Certificate</label>
	<div class="col-sm-5">
		<input type="file" name="certificate" id="certificate" class="form-control" value="{{ isset($higherEducation->certificate) ? $higherEducation->certificate : old('certificate') }}">
	</div>
</div>
<div class="form-group">
	<label for="qualification_id" class="col-sm-3 control-label">Qualification Level</label>
	<div class="col-sm-5">
	<select name="qualification_id" id="qualification_id" class="form-control">
		<option value="{{ isset($higherEducation->qualification_id) ? $higherEducation->qualification_id : old('qualification_id') }}">-Select Qualification Level-</option>
		@foreach($qualifications as $qualification)
		<option value="{{ $qualification->id }}">{{ $qualification->name }}</option>
		@endforeach
	</select>		
	</div>
</div>