@extends('layouts.app')

@section('title', 'Higher Education')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2">
            @include('partials.sidebar')
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Higher Education</div>

                <div class="panel-body">
                    @if($higherEducations->count())
                    <table class="table table-bordered table-hover table-responsive">
                        <thead>
                            <tr>
                                <th class="text-right">SN</th>
                                <th>Registration Number</th>
                                <th>Program Name</th>
                                <th>Graduation Year</th>
                                <th>Overall GPA</th>
                                <th>Qualification Level</th>
                                <th>Certificate</th>
                                <th>Created Date</th>
                                <th>Updated Date</th>
                                <th colspan="2">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $i = 1 @endphp
                            @foreach($higherEducations as $higherEducation)
                            <tr>
                                <td class="text-right">{{ $i++ }}.</td>
                                <td>
                                    <a href="#">
                                        {{ $higherEducation->registration_number }}
                                    </a>
                                </td>
                                <td>{{ $higherEducation->program_name }}</td>
                                <td>{{ $higherEducation->graduation_year }}</td>
                                <td>{{ $higherEducation->overall_gpa }}</td>
                                <td>{{ $higherEducation->qualification->name }}</td>
                                <td>{{ str_limit($higherEducation->certificate, 10) }}</td>
                                <td>{{ $higherEducation->created_at->diffForHumans() }}</td>
                                <td>{{ $higherEducation->updated_at->diffForHumans() }}</td>
                                <td>
                                    <a href="#" class="btn btn-success btn-sm">Edit</a>
                                </td>
                                <td>
                                    <form action="#" method="POST" role="form">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE')}}
                                        <a class="btn btn-danger btn-sm" data-toggle="modal" href='#delete-he-{{ $higherEducation->id }}'>Delete</a>
                                        <div class="modal fade" id="delete-he-{{ $higherEducation->id }}">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h4 class="modal-title">{{ $higherEducation->program_name }}</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        Are you sure you want to delete {{ $higherEducation->program_name }} ?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                        <button type="submit" class="btn btn-danger">Delete</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                    <p class="text-center">
                        You have not added any higher education information
                    </p>
                    @endif
                </div>
                <div class="panel-footer">
                    <a href="/higher-education/create" class="btn btn-primary">Add Information</a>
                </div>
            </div>
        </div>
        <div class="col-sm-2">
                
                @include('partials.applicant.progress', ['user' => Auth::user()])
                
            </div> 
    </div>
</div>
@endsection
