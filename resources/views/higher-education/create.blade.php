@extends('layouts.app')

@section('title', 'Add Higher Education')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2">
            @include('partials.sidebar')
        </div>
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">Add Higher Education</div>

                <div class="panel-body">
                    @include('errors.list')
                    <form action="/higher-education" method="POST" class="form-horizontal" role="form" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        @include('higher-education._form')

                        <div class="form-group">
                            <div class="col-sm-6 col-sm-offset-3">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
