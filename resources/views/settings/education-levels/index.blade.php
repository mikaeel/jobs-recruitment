@extends('layouts.app')

@section('title', 'Education Levels')

@section('content')

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-2">
				@include('dashboard.partials.sidebar')
			</div>
			<div class="col-sm-10">
				<table class="table table-bordered table-responsive">
					<thead>
						<tr>
							<th>SN</th>
							<th>Name</th>
							<th>Date</th>
							<th>Edit</th>
							<th>Delete</th>
						</tr>
					</thead>
					<tbody>
						@php $i = 1;@endphp
						@foreach($educationLevels as $educationLevel)
							<tr>
								<td>{{ $i++ }}.</td>
								<td>{{ $educationLevel->name }}</td>
								<td>{{ $educationLevel->created_at }}</td>
								<td><a href="#">Edit</a></td>
								<td><a href="#">Delete</a></td>
							</tr>
						@endforeach	
					</tbody>	
				</table>
			</div>
		</div>
	</div>

@endsection
