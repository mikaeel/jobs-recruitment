@extends('layouts.app')

@section('title', 'Job Categories')

@section('content')

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-2">
				@include('dashboard.partials.sidebar')
			</div>
			<div class="col-sm-10">

				<a href="{{ route('job-categories.create') }}" class="btn btn-primary">Add</a>
				<br><br>

				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Job Categories</h3>
					</div>
					<div class="panel-body">
						
						@if($jobCategories->count())
						<table class="table table-bordered table-responsive">
							<thead>
								<tr>
									<th>SN</th>
									<th>Name</th>
									<th>Date</th>
									<th>Edit</th>
									<th>Delete</th>
								</tr>
							</thead>
							<tbody>
								@php $i = 1;@endphp
								@foreach($jobCategories as $jobCategory)
									<tr>
										<td>{{ $i++ }}.</td>
										<td>{{ $jobCategory->name }}</td>
										<td>{{ $jobCategory->created_at->diffForHumans() }}</td>
										<td><a href="#">Edit</a></td>
										<td><a href="#">Delete</a></td>
									</tr>
								@endforeach	
							</tbody>	
						</table>
						@else
							<p class="text-center">No Job Category</p>
						@endif

					</div>
				</div>
			
			</div>
		</div>
	</div>

@endsection
