<div class="form-group">
    <label for="name" class="col-sm-1 col-sm-offset-1 control-label">Name</label>
    <div class="col-sm-7">
        <input type="text" name="name" id="name" value="{{ $computerSkills->name or old('name') }}"
               class="form-control" required>
    </div>
</div>