@extends('layouts.app')

@section('title', 'Nationalities')

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-sm-2">
			@include('dashboard.partials.sidebar')
		</div>
		<div class="col-sm-10">
			<div class="panel panel-default">
				<div class="panel-heading">
					<a href="{{ route('settings.nationalities.create')}}" class="btn btn-primary">Create New</a>
				</div>
				<div class="panel-body">
				@if($nationalities->count())
					<table class="table table-bordered table-responsive">
						<thead>
							<tr>
								<th>SN</th>
								<th>Name</th>
								<th>Date</th>
								<th>Edit</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody>
							@php $i = 1;@endphp
							@foreach($nationalities as $nationalitie)
							<tr>
								<td>{{ $i++ }}.</td>
								<td>{{ $nationalitie->name }}</td>
								<td>{{ $nationalitie->created_at->diffForHumans() }}</td>
								<td><a href="#">Edit</a></td>
								<td><a href="#">Delete</a></td>
							</tr>
							@endforeach	
						</tbody>	
					</table>
					@else
					<div class="alert alert-info">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<h4>No Nationalities has been added</h4>
					</div>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
