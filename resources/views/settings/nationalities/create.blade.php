@extends('layouts.app')

@section('title', 'Competences')

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-sm-2">
			@include('dashboard.partials.sidebar')
		</div>
		<div class="col-sm-10">

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">New Competences</h3>
				</div>
				<div class="panel-body">

					@include('errors.list')

					<form action="{{ route('settings.nationalities.store') }}" method="post" class="form-horizontal">
						{{ csrf_field() }}

						@include('settings.nationalities._form')

						<div class="form-group">
							<div class="col-sm-6 col-sm-offset-3">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>	
						</div>
					</form>

				</div>
			</div>
			
		</div>
	</div>
</div>

@endsection
