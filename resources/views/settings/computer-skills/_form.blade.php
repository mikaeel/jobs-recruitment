<div class="form-group">
    <label for="name" class="col-sm-1 col-sm-offset-1 control-label">Name</label>
    <div class="col-sm-7">
        <input type="text" name="name" id="name" value="{{ $computerSkills->name or old('name') }}"
               class="form-control">
    </div>
</div>

<div class="form-group">
    <label for="description" class="col-sm-1 col-sm-offset-1 control-label">Description</label>
    <div class="col-sm-7">
        <textarea name="description" col="5" row="5" id="description" class="form-control">{{ $computerSkills->description or old('description') }}
        </textarea> 
    </div>
</div>

