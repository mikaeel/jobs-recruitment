@extends('layouts.app')

@section('title', 'Computer Skills')

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-sm-2">
			@include('dashboard.partials.sidebar')
		</div>
		<div class="col-sm-10">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">New Computer Skills</h3>
				</div>
				<div class="panel-body">
					@include('errors.list')
					<form action="{{route('settings.computer-skills.store')}}" method="POST" class="form-horizontal">
						{{ csrf_field() }}
						@include('settings.computer-skills._form')
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-2 ">
								<button type="submit" class="btn btn-primary">Save</button>
							</div> 
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
