@extends('layouts.app')

@section('title', 'Computer Skills')

@section('content')

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-2">
				@include('dashboard.partials.sidebar')
			</div>
			<div class="col-sm-10">
			<form action="{{route('settings.computer-skills.update',$computerSkills->id)}}" method="POST" class="form-horizontal">
			    {{ csrf_field() }}
			    {{ method_field('PATCH') }}
				@include('settings.computer-skills._form')
			<div class="form-group">
			<div class="col-sm-1 col-sm-offset-5 ">
				<button type="submit" class="btn btn-primary">Edit</button>
			</div> 
			</div>
			</form>
			</div>
		</div>
	</div>

@endsection
