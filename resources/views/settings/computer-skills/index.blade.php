@extends('layouts.app')

@section('title', 'Computer Skills')

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-sm-2">
			@include('dashboard.partials.sidebar')
		</div>
		<div class="col-sm-10">
			<div class="panel panel-default">
				<div class="panel-heading">
					<a href="{{ route('settings.competences.create') }}" class="btn btn-primary">Create New</a>
				</div>
				<div class="panel-body">
					@if($computerSkills->count())
					<table class="table table-bordered table-responsive">
						<thead>
							<tr>
								<th>SN</th>
								<th>Name</th>
								<th>Description</th>
								<th>Date</th>
								<th>Edit</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody>
							@php $i = 1;@endphp
							@foreach($computerSkills as $computerSkill)
							<tr>
								<td>{{ $i++ }}.</td>
								<td>{{ $computerSkill->name }}</td>
								<td>{{ str_limit($computerSkill->description, 70) }}</td>
								<td>{{ $computerSkill->created_at->diffForHumans() }}</td>
								<td><a href="{{route('settings.computer-skills.edit',$computerSkill->id)}}">Edit</a></td>
								<td>
									<a class="btn btn-danger btn-sm" data-toggle="modal"
									href='#delete-{{ $computerSkill->id }}'><i class="fa fa-trash"></i> Delete</a>
									<div class="modal fade" id="delete-{{ $computerSkill->id }}">
										<div class="modal-dialog">
											<form method="POST" action="{{ route('settings.computer-skills.destroy',$computerSkill->id) }}">
												{{ csrf_field() }}
												{{ method_field('DELETE') }}

												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal"
														aria-hidden="true">&times;</button>
														<h4 class="modal-title">
															Delete {{ $computerSkill->name }}</h4>
														</div>
														<div class="modal-body">
															Delete
															permanently {{ $computerSkill->name }}                                                                 ?
														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-default"
															data-dismiss="modal">Cancel
														</button>
														<button type="submit" class="btn btn-danger"><i
															class="fa fa-trash"></i> Delete
														</button>
													</div>
												</div>
											</form>
										</div>
									</div>
								</td>
							</tr>
							@endforeach	
						</tbody>	
					</table>
					@else
					<div class="alert alert-info">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<strong>No data has been added for Computer Skills</strong>
					</div>
					@endif	
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
