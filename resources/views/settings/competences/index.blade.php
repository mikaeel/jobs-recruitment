@extends('layouts.app')

@section('title', 'Competences')

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-sm-2">
			@include('dashboard.partials.sidebar')
		</div>
		<div class="col-sm-10">
			<div class="panel panel-default">
				<div class="panel-heading">
					<a href="{{ route('settings.competences.create')}}" class="btn btn-primary">Create New</a>
				</div>
				<div class="panel-body">
				@if($competences->count())
					<table class="table table-bordered table-responsive">
						<thead>
							<tr>
								<th>SN</th>
								<th>Name</th>
								<th>Date</th>
								<th>Edit</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody>
							@php $i = 1;@endphp
							@foreach($competences as $competence)
							<tr>
								<td>{{ $i++ }}.</td>
								<td>{{ $competence->name }}</td>
								<td>{{ $competence->created_at->diffForHumans() }}</td>
								<td><a href="#">Edit</a></td>
								<td><a href="#">Delete</a></td>
							</tr>
							@endforeach	
						</tbody>	
					</table>
					@else
					<div class="alert alert-info">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<h4>No Competence has been added</h4>
					</div>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
