<div class="form-group">
    <label for="name" class="col-sm-2 col-sm-offset-1 control-label">Competence name</label>
    <div class="col-sm-7">
        <input type="text" name="name" id="name" value="{{ $competences->name or old('name') }}"
               class="form-control" required>
    </div>
</div>