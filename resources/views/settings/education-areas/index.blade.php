@extends('layouts.app')

@section('title', 'Education Areas')

@section('content')

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-2">
				@include('dashboard.partials.sidebar')
			</div>
			<div class="col-sm-10">
				<table class="table table-bordered table-responsive">
					<thead>
						<tr>
							<th>SN</th>
							<th>Name</th>
							<th>Date</th>
							<th>Edit</th>
							<th>Delete</th>
						</tr>
					</thead>
					<tbody>
						@php $i = 1;@endphp
						@foreach($educationAreas as $educationArea)
							<tr>
								<td>{{ $i++ }}.</td>
								<td>{{ $educationArea->name }}</td>
								<td>{{ $educationArea->created_at }}</td>
								<td><a href="#">Edit</a></td>
								<td><a href="#">Delete</a></td>
							</tr>
						@endforeach	
					</tbody>	
				</table>
			</div>
		</div>
	</div>

@endsection
