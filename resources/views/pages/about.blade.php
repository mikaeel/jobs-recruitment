@extends('layouts.app')

@section('title', 'About')

@section('content')

<div class="container">
  <div class="page-header">
    <h1>About</h1>
  </div>  
  <div class="row">
  	<div class="col-sm-12">
  		<p class="lead">
  			{{ Config('app.name') }} is an online system that helps you to apply for job anywhere you are.
  		</p>
  	</div>
  </div>
</div>

@endsection
