@extends('layouts.app')

@section('title', 'Contact')

@section('content')

<div class="container">
	<div class="page-header">
	  <h1>Contact</h1>
	</div>	
	<div class="row">
		<div class="col-sm-12">
			<p class="lead">
				If you have anything please contact us though the university website
			</p>
		</div>
	</div>
</div>

@endsection
