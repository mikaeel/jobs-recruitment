<?php

use App\Job;

Route::get('/', function () {
    $jobs = Job::latest()->limit(5)->get();
    return view('welcome', compact('jobs'));
});

Route::get('about', 'PagesController@about')->name('about');
Route::get('contact', 'PagesController@contact')->name('contact');
Route::get('support', 'PagesController@support')->name('support');

Auth::routes();

Route::get('register/confirm/{token}', 'Auth\RegisterController@confirmEmail');

Route::get('home', 'HomeController@index')->name('home');

Route::get('jobs/category/{category}', 'JobCategoryController@show')->name('job-category.show');

Route::get('jobs', 'JobController@index')->name('jobs.index');
Route::get('jobs/{job}', 'JobController@show')->name('jobs.show');

Route::group(['middleware' => 'auth'], function () {
    Route::get('personal-details', 'UserController@show');

    Route::patch('personal-details', 'UserController@update');

    Route::resource('employment-history', 'EmploymentHistoryController');

    Route::get('computer-skills', 'ComputerSkillController@index')->name('computer-skills.index');
    Route::post('computer-skills', 'ComputerSkillController@store')->name('computer-skills.store');
    Route::delete('computer-skills/{id}', 'ComputerSkillController@destroy')->name('computer-skills.destroy');

    Route::get('education-background', 'EducationBackgroundController@index')->name('education-background.index');
    Route::get('education-background/create', 'EducationBackgroundController@create')->name('education-background.create');
    Route::post('education-background', 'EducationBackgroundController@store')->name('education-background.post');

    Route::resource('references', 'ReferenceController');

    Route::resource('professional-training', 'ProfessionalTrainingController');

    Route::resource('interests', 'InterestController');

    Route::resource('additional-skills', 'AdditionalSkillController');

    Route::get('my-cv', 'UserController@myCv')->name('my-cv');
});

/*
/-----------------------------------
/ Job Application
/ The process of applying for a job
*/
Route::post('jobs/{slug}/{job}/apply', 'UserController@storeJob')->name('job.apply');

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
|
| Here is where you can register dashboard routes for your application. 
|
*/

Route::group(['namespace' => 'Dashboard', 'middleware' => ['auth', 'hr']], function () {

    // Dashboard
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');

    // Jobs
    Route::get('dashboard/jobs', 'JobController@index')->name('dashboard.jobs.index');
    Route::get('dashboard/jobs/create', 'JobController@create')->name('dashboard.jobs.create');
    Route::get('dashboard/jobs/{job}/edit', 'JobController@edit')->name('dashboard.jobs.edit');
    Route::patch('dashboard/jobs/{job}', 'JobController@update')->name('dashboard.jobs.update');
    Route::post('dashboard/jobs', 'JobController@store')->name('dashboard.jobs.store');
    Route::get('dashboard/jobs/{slug}/{job}', 'JobController@show')->name('dashboard.jobs.show');
    Route::get('dashboard/jobs/{slug}/{job}/applicants/{applicant}', 'JobController@applicants')->name('dashboard.jobs.show.applicants.show');

    // Applicants
    Route::get('dashboard/applicants', 'ApplicantController@index')->name('dashboard.applicants.index');
    Route::get('dashboard/applicants/{applicant}', 'ApplicantController@show')->name('dashboard.applicants.show');
    
    Route::get('dashboard/applicants/{applicant}/profile', 'ApplicantController@profile')->name('dashboard.applicants.profile');

    // Interviews
    Route::post('dashboard/interviews/{job}/{applicant}', 'InterviewController@store')->name('dashboard.interviews.store');

    // Qualifications
    Route::resource('dashboard/qualification-level', 'QualificationsController');

});

// Settings
Route::group(['namespace' => 'Settings', 'middleware' => 'auth'], function () {
    Route::resource('settings/job-categories', 'JobCategoryController');

    Route::get('settings/computer-skills', 'ComputerSkillController@index')->name('settings.computer-skills.index');
    Route::get('settings/computer-skills/create', 'ComputerSkillController@create')->name('settings.computer-skills.create');
    Route::post('settings/computer-skills/', 'ComputerSkillController@store')->name('settings.computer-skills.store');
    Route::get('settings/computer-skill/{computerSkill}/edit', 'ComputerSkillController@edit')->name('settings.computer-skills.edit');
    Route::patch('settings/computer-skills/{computerSkill}', 'ComputerSkillController@update')->name('settings.computer-skills.update');
    Route::delete('settings/computer-skills/{computer-skill}', 'ComputerSkillController@destroy')->name('settings.computer-skills.destroy');
    
    Route::delete('settings/computer-skills/{computerSkill}', 'ComputerSkillController@destroy')->name('settings.computer-skills.destroy');
    Route::get('settings/competences', 'CompetenceController@index')->name('settings.competences.index');
    Route::get('settings/competences/create', 'CompetenceController@create')->name('settings.competences.create');
    Route::post('settings/competences/store', 'CompetenceController@store')->name('settings.competences.store');

    Route::get('settings/education-levels', 'EducationLevelController@index')->name('settings.education-levels.index');
    Route::get('settings/education-areas', 'EducationAreaController@index')->name('settings.education-areas.index');
    //Nationalities
    Route::get('settings/nationalities', 'NationalitiesController@index')->name('settings.nationalities.index');
    Route::get('settings/nationalities/create', 'NationalitiesController@create')->name('settings.nationalities.create');
    Route::post('settings/nationalities/store', 'NationalitiesController@store')->name('settings.nationalities.store');
});

Route::group(['middleware' => 'auth'], function () {
    Route::post('primary-school', 'PrimarySchoolController@store')->name('primary-school.store');
    Route::post('ordinary-level', 'OrdinaryLevelController@store')->name('ordinary-level.store');
    Route::post('advanced-level', 'AdvancedLevelController@store')->name('advanced-level.store');
    Route::get('my-applications', function () {
       $jobs = auth()->user()->job_applications;
       return view('jobs.my-applications', compact('jobs'));
   })->name('my-applications');
    Route::resource('higher-education', 'HigherEducations');
});

/** Reports */
Route::group(['middleware' => 'auth', 'namespace' => 'Reports'], function () {
    Route::post('reports/jobs/{job}', 'ExportController@jobApplicants')->name('reports.jobs.applicants');

    Route::get('reports/jobs-with-applicants', 'ExportController@jobsWithApplicants')->name('reports.jobs-with-applicants');

    Route::get('reports/jobs-with-applicants-call-for-interview', 'ExportController@jobsWithApplicantsCallForInterview')->name('reports.jobs-with-applicants-call-for-interview');

    Route::get('reports/jobs-with-applicants-not-call-for-interview', 'ExportController@jobsWithApplicantsNotCallForInterview')->name('reports.jobs-with-applicants-not-call-for-interview');
});

Route::post('download', 'DownloadController@download')->name('download');
Route::post('preview', 'DownloadController@preview')->name('preview');
