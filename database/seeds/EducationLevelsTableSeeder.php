<?php

use App\EducationLevel;
use Illuminate\Database\Seeder;

class EducationLevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('education_levels')->delete();

        factory('App\EducationLevel', 10)->create();
    }
}
