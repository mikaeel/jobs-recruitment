<?php

use App\Competence;
use Illuminate\Database\Seeder;

class CompetencesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('competences')->delete();

        Competence::create(['name' => 'Poor', 'slug' => 'poor']);
        Competence::create(['name' => 'Good', 'slug' => 'good']);
        Competence::create(['name' => 'Moderate', 'slug' => 'moderate']);
        Competence::create(['name' => 'Very Good', 'slug' => 'very-good']);
        Competence::create(['name' => 'Excellent', 'slug' => 'excellent']);
    }
}
