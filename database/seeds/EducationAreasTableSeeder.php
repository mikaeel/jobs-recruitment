<?php

use App\EducationArea;
use Illuminate\Database\Seeder;

class EducationAreasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('education_areas')->delete();

        factory('App\EducationArea', 10)->create();
    }
}
