<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('users')->delete();
        User::create([
        	'email' => 'hr@udom.ac.tz',
        	'password' => bcrypt('secret'),
        	'verified' => true,
        ]);
    }
}
