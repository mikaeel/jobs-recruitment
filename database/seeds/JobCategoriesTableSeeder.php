<?php


use App\JobCategory;
use Illuminate\Database\Seeder;

class JobCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('job_categories')->delete();
        $user = factory('App\User')->create();

        JobCategory::unguard();
        JobCategory::create(['name' =>'Academic', 'slug' => 'academic', 'user_id' => $user->id]);
        JobCategory::create(['name' =>'Administrative', 'slug' => 'administrative', 'user_id' => $user->id]);
    }
}
