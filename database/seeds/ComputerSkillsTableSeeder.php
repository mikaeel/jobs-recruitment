<?php

use Illuminate\Database\Seeder;

class ComputerSkillsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('computer_skills')->delete();

        factory('App\ComputerSkill', 10)->create();
    }
}
