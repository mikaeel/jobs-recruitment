<?php

use App\Job;
use Illuminate\Database\Seeder;

class ApplicantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create 100 applicants
        factory('App\User', 100)->create([
            'password' => bcrypt('12345'),
            'verified' => true,
        ]);
    }
}
