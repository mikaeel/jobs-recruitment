<?php

use App\Job;
use Illuminate\Database\Seeder;

class JobVacanciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('job_vacancies')->delete();

        factory('App\Job', 20)->create();
    }
}
