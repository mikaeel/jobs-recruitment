<?php

use App\User;
use Spatie\Permission\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();

        $role = Role::create(['name' => 'human-resource']);

        User::first()->assignRole($role->name);
    }
}
