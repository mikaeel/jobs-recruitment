<?php

use App\Job;
use App\User;
use App\UserProgress;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        $user = User::create([
            'email' => 'hr@udom.ac.tz',
            'password' => bcrypt('secret'),
            'verified' => true,
        ]);     

        $user->progress()->save(new UserProgress);   
        $this->call(RolesTableSeeder::class);
        $this->call(CompetencesTableSeeder::class);
        
        // $this->call(UsersTableSeeder::class);
        // $this->call(ComputerSkillsTableSeeder::class);
        // $this->call(EducationAreasTableSeeder::class);
        // $this->call(EducationLevelsTableSeeder::class);
        // $this->call(JobCategoriesTableSeeder::class);
        // $this->call(JobVacanciesTableSeeder::class);
        // $this->call(ApplicantsTableSeeder::class);


        // // Attach Applicants to Jobs
        // $applicantsIds = User::pluck('id');
        // $jobs = Job::get();

        // $jobs->each(function ($job) use ($applicantsIds) {
        //     $job->applicants()->attach($applicantsIds);
        // });
    }
}
