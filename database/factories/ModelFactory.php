<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'first_name' => $faker->firstName,
        'middle_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'gender' => 'Male',
        'marital_status' => 'Single',
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'verified' => true,
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\JobCategory::class, function (Faker\Generator $faker) {
    $name = ucfirst($faker->word);

    return [
        'name' => $name,
        'slug' => str_slug($name),
        'user_id' => function() {
            return factory('App\User')->create()->id;
        }
    ];
});

$factory->define(App\Job::class, function (Faker\Generator $faker) {
    $title = $faker->jobTitle;
    return [
        'title' => $title,
        'slug' => str_slug($title, '-'),
        'body' => $faker->paragraph,
        'user_id' => function () {
            return factory('App\User')->create()->id;
        },
        'status' => $faker->boolean(),
        'closed_at' => Carbon\Carbon::now()->addWeeks(4),
        'job_category_id' => function() {
            return factory('App\JobCategory')->create()->id;
        },
    ];
});

$factory->define(App\ComputerSkill::class, function (Faker\Generator $faker) {
    $name = $faker->unique()->jobTitle;
    return [
        'name' => $name,
        'slug' => str_slug($name, '-'),
        'description' => $faker->paragraph,
        'user_id' => function () {
            return factory('App\User')->create()->id;
        },
    ];
});

$factory->define(App\EducationLevel::class, function (Faker\Generator $faker) {
    $name = $faker->unique()->jobTitle;
    return [
        'name' => $name,
        'slug' => str_slug($name, '-'),
        'description' => $faker->paragraph,
        'user_id' => function () {
            return factory('App\User')->create()->id;
        },
    ];
});

$factory->define(App\EducationArea::class, function (Faker\Generator $faker) {
    $name = $faker->unique()->jobTitle;
    return [
        'name' => $name,
        'slug' => str_slug($name, '-'),
        'description' => $faker->paragraph,
        'user_id' => function () {
            return factory('App\User')->create()->id;
        },
    ];
});
