<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHigherEducationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('higher_educations', function (Blueprint $table) {
            $table->increments('id');
            $table->text('registration_number');
            $table->text('program_name');
            $table->unsignedSmallInteger('graduation_year');
            $table->double('overall_gpa', 2, 1);
            $table->text('certificate');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('qualification_id')->unsigned()->index()->uniqid();
            $table->timestamps();
            $table->foreign('qualification_id')->references('id')->on('qualifications');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('higher_educations');
    }
}
