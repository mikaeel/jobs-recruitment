<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProgressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_progresses', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('personal_details')->default(false);
            $table->boolean('education_background')->default(false);
            $table->boolean('employment_history')->default(false);
            $table->boolean('professional_training')->default(false);
            $table->boolean('additional_skills')->default(false);
            $table->boolean('computer_skills')->default(false);
            $table->boolean('interests')->default(false);
            $table->boolean('references')->default(false);

            $table->integer('user_id')->unsigned()->index();

            $table->timestamps();

            $table->foreign('user_id')
                  ->references('id')
                  ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_progresses');
    }
}
